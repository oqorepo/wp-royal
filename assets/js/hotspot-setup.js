$(document).ready(function () {
    $('#hotspotImg').hotSpot();
    $('#video').YTPlayer({
        fitToBackground: true,
        videoId: ''+fullvideo.video_url+'',
        pauseOnScroll: false,
        height: '100%',
        width: '100%',
        anchor: 'center,center',
        playerVars: {
            modestbranding: 0,
            autoplay: 1,
            controls: 0,
            showinfo: 0,
            wmode: 'transparent',
            branding: 0,
            rel: 0,
            autohide: 0,
            origin: window.location.origin
        }
    });
//console.log(fullvideo.video_url);
});
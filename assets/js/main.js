function agregarPuntos(str) {
	var parts = (str + "").split("."),
		main = parts[0],
		len = main.length,
		output = "",
		first = main.charAt(0),
		i;

	if (first === '-') {
		main = main.slice(1);
		len = main.length;
	} else {
		first = "";
	}
	i = len - 1;
	while (i >= 0) {
		output = main.charAt(i) + output;
		if ((len - i) % 3 === 0 && i > 0) {
			output = "." + output;
		}
		--i;
	}
	// put sign back
	output = first + output;
	// put decimal part back
	if (parts.length > 1) {
		output += "." + parts[1];
	}
	return output;
}

$(document).ready(function () {
	/* 
	------------------------------------------------------------------
	Tracking
	------------------------------------------------------------------
	*/
	function logSource(sbData) {
		//console.log('Cookies are set! Your source is: ${sbData.current.src}');
	}
	sbjs.init({
		promocode: true,
		callback: logSource
	});
	//Parametros de URL
	$.urlParam = function (name) {
		var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
		if (results) {
			return results[1] || 0;
		}
	}
	//Parametros de URL identificadores de google
	if ($.urlParam('sce')) {
		localStorage.setItem('gadsSce', $.urlParam('sce'));
		localStorage.setItem('gadsAd', $.urlParam('crea'));
		localStorage.setItem('gadsIdAd', $.urlParam('cid'));
		localStorage.setItem('gadsNet', $.urlParam('net'));
	}

	/* 
	------------------------------------------------------------------
	PRELOAD
	------------------------------------------------------------------
	*/
	$(window).on('load', function () {
		$('.cd-loader').fadeOut('slow', function () {
			$(this).remove();
		});
	});

	$('[data-toggle="tooltip"]').tooltip();
	/* 
	------------------------------------------------------------------
		Formato del Menu principal
	------------------------------------------------------------------
	*/
	// Primary
	$('.bk-navbar__nav .nav-item').addClass('d-flex align-self-stretch');
	$('.bk-navbar__nav .nav-link').addClass('align-self-center');
	//footer
	$('#menu-footer-menu li').addClass('footer-nav-item d-flex align-self-stretch');
	$('#menu-footer-menu li a').addClass('footer-nav-link d-flex align-self-stretch');

	/* 
	------------------------------------------------------------------
		Hamburger trigger
	------------------------------------------------------------------
	*/
	$('.hamburger').on('click', function () {
		$(this).toggleClass('is-active');
	});

	/* 
	------------------------------------------------------------------
		MEGA MENÚ
	------------------------------------------------------------------
	*/
	$(window).resize(function () {
		if ($(window).width() >= 980) {
			$(".navbar .dropdown-toggle").hover(function () {
				$(this).parent().toggleClass("show");
				$(this).parent().find(".dropdown-menu").toggleClass("show");
			});
			// ocultar el menú al salir el mouse
			$(".navbar .dropdown-menu").mouseleave(function () {
				$(this).removeClass("show");
			});
		}
	});
	$('#menu-primary').addClass('justify-content-between');
	$('#menu-primary > li > a').addClass('bk-icon');
	$('#menu-primary > li:last-child > a').removeClass('bk-icon');

	$('.wp-megamenu-sub-menu .wpmm-tab-btns li').addClass('bk-item-vertical');
	$('.wp-megamenu-sub-menu .wpmm-tab-btns li a').addClass('bk-item-vertical__link');
	$('.bk-wc--buttons a').addClass('bk--btn bk--btn__line bk--btn__bg-none');
	$('.wpmm-vertical-tabs-nav .wpmm-tab-btns li:first-child a').text("Todas");
	/* 
	------------------------------------------------------------------
		Página de Concesionarios
	------------------------------------------------------------------
	*/
	var concesionario_selected = $('#js_selector_regiones_ces').val();
	//console.log('Concesionario seleccionado: ' + concesionario_selected);

	$('.bk-distribuidores li').each(function () {
		if ($(this).hasClass(concesionario_selected)) {
			$(this).removeClass('d-none');
			$(this).addClass('bk-ces-active');
			$('.bk-ces-active').first().find('.marker').addClass('active');
			var nameId = $('.bk-ces-active .marker.active').find('h4').text().replace(/ /g, '').toLowerCase();
			$('#' + nameId).show();
		}
	});

	$('.marker.active').show();

	$('#js_selector_regiones_ces').change(function () {
		$('.bk-dealer--selector').addClass('col-md-6 col-lg-3 bk-shadow');
		$('.bk-dealer--contenido, .bk-dealer--map').removeClass('d-none');

		$('.bk-distribuidores').find('.marker.active').removeClass('active');
		$('.bk-distribuidores li').addClass('d-none');
		$('.bk-distribuidores li').removeClass('bk-ces-active');
		var change_concesionario_selected = $(this).val();
		//console.log('Región seleccionada: ' + change_concesionario_selected);
		$('.bk-distribuidores li').each(function () {
			if ($(this).hasClass(change_concesionario_selected)) {
				$(this).removeClass('d-none');
				$(this).addClass('bk-ces-active');
				$('.bk-ces-active').find('.marker').first().addClass('active');
			}
		});
		var markerName = $('.bk-ces-active .marker.active .bk-marker').attr('data-name'),
			concName = $('#' + markerName);
		//console.log('CES seleccionado: ' + markerName);
		$('.bk-dealer--content').hide();
		$(concName).show();
	});

	$('.bk-marker').on('click', function () {
		$('.bk-distribuidores').find('.marker.active').removeClass('active');
		var markerName = $(this).attr('data-name'),
			concName = $('#' + markerName);
		//console.log('Nuevo CES seleccionado: ' + markerName);
		$('.bk-dealer--content').hide();
		$(concName).show();
		$(this).parent('.marker').addClass('active');
	});
	/* 
	------------------------------------------------------------------
		Woocommerce layout tienda
	------------------------------------------------------------------
	*/
	var btnRounded = $('.archive .bk-woocommerce').find('.product').find('.compare.button');
	btnRounded.addClass('bk-button--rounded');
	btnRounded.text('+');
	btnRounded.attr({
		'data-toggle': "tooltip",
		'data-placement': "right",
		title: "Añadir al comparador"
	});

	var knowHim = $('.bk-woocommerce').find('.woocommerce-loop-product__link').find('.conocela');
	knowHim.addClass('bk-woocommerce--shop__btn bk--btn bk--btn__primary d-block');

	var wcTitle = $('.bk-woocommerce').find('.woocommerce-loop-product__link').find('.woocommerce-loop-product__title');
	wcTitle.addClass('bk-woocommerce--shop__title text-center');

	$('.cotizar.bk-woocommerce--shop__btn').addClass('bk-hide');

	$('.bk-woocommerce .product').on({
		mouseenter: function () {
			//stuff to do on mouse enter
			$(this).find('.cotizar.bk-woocommerce--shop__btn').removeClass('bk-hide');
			//(this).css('border','1px solid black');
		},
		mouseleave: function () {
			//stuff to do on mouse leave
			$(this).find('.cotizar.bk-woocommerce--shop__btn').addClass('bk-hide');
			//$(this).css('border','none');
		}
	});

	$('.post-type-archive-product.archive').removeClass('woocommerce woocommerce-page');
	$('.archive .bk-woocommerce .products').addClass('row');
	$('.archive .bk-woocommerce').find('.product.status-publish').addClass('col-sm-6 col-md-4 col-lg-3 pt-2 pb-4');

	$('.bk-wc-category--list__item').on('click', function () {
		var product_id = $(this).attr('id'),
			acUrl = window.location.protocol + "//" + window.location.host + window.location.pathname;
		//console.log(acUrl);
		$('.bk-woocommerce').find('.product.status-publish').hide();
		$('.product.product_cat-' + product_id).show();
		$('.bk-wc-category--list__item').removeClass('active');
		$(this).addClass('active');
		history.pushState(acUrl, product_id, acUrl + "?m=" + product_id + "");
	});

	$('#todas').on('click', function () {
		$('.bk-woocommerce').find('.product.status-publish').show();
	});

	$('.archive .bk-woocommerce .product.type-product').on({
		mouseenter: function () {
			$(this).addClass('bk-product--is-hover');
		},
		mouseleave: function () {
			$(this).removeClass('bk-product--is-hover');
		}
	});

	$('.wpmm-col.wpmm-col-4').addClass('bk-menu--promo');
	/*
	------------------------------------------------------------------
		pushData para metricas
		(las demas estan en los flujos de cotizacion, lanzamiento
		de cotizador y pagina de producto)
	------------------------------------------------------------------
	*/

	if (window.location.pathname == '/') {
		dataLayer.push({
			'event': 'inicio',
			'procutId': $('input[name="product_id"]').val(),
			'fuente': sbjs.get.current.src,
			'medio': sbjs.get.current.mdm,
			'campaign': sbjs.get.current.cmp,
			'content': sbjs.get.current.cnt,
			'terminos': sbjs.get.current.trm,
			'codigo': sbjs.get.promo.code,
			'gadsSce': localStorage.getItem('gadsSce'),
			'gadsAd': localStorage.getItem('gadsAd'),
			'gadsIdAd': localStorage.getItem('gadsIdAd'),
			'gadsNet': localStorage.getItem('gadsNet')
		});
	}

	if (window.location.pathname == '/concesionarios/') {
		dataLayer.push({
			'event': 'concesionarios',
			'fuente': sbjs.get.current.src,
			'medio': sbjs.get.current.mdm,
			'campaign': sbjs.get.current.cmp,
			'content': sbjs.get.current.cnt,
			'terminos': sbjs.get.current.trm,
			'codigo': sbjs.get.promo.code,
			'gadsSce': localStorage.getItem('gadsSce'),
			'gadsAd': localStorage.getItem('gadsAd'),
			'gadsIdAd': localStorage.getItem('gadsIdAd'),
			'gadsNet': localStorage.getItem('gadsNet')
		});
	}

	if (window.location.pathname == '/promociones/') {
		dataLayer.push({
			'event': 'promociones',
			'fuente': sbjs.get.current.src,
			'medio': sbjs.get.current.mdm,
			'campaign': sbjs.get.current.cmp,
			'content': sbjs.get.current.cnt,
			'terminos': sbjs.get.current.trm,
			'codigo': sbjs.get.promo.code,
			'gadsSce': localStorage.getItem('gadsSce'),
			'gadsAd': localStorage.getItem('gadsAd'),
			'gadsIdAd': localStorage.getItem('gadsIdAd'),
			'gadsNet': localStorage.getItem('gadsNet')
		});
	}

	/*
	------------------------------------------------------------------
		Pagina de Financiamiento
	------------------------------------------------------------------
	*/

	if (window.location.pathname.indexOf("financiamiento") > -1) {

		dataLayer.push({
			'event': 'financiamiento',
			'fuente': sbjs.get.current.src,
			'medio': sbjs.get.current.mdm,
			'campaign': sbjs.get.current.cmp,
			'content': sbjs.get.current.cnt,
			'terminos': sbjs.get.current.trm,
			'codigo': sbjs.get.promo.code,
			'gadsSce': localStorage.getItem('gadsSce'),
			'gadsAd': localStorage.getItem('gadsAd'),
			'gadsIdAd': localStorage.getItem('gadsIdAd'),
			'gadsNet': localStorage.getItem('gadsNet')
		});

		$(function () {
			$('#slider-range').slider({
				range: true,
				min: 800000,
				max: 16000000,
				values: [800000, 16000000],
				step: 50000,
				slide: function (event, ui) {
					$("#amount").val("$" + agregarPuntos(ui.values[0]) + " - $" + agregarPuntos(ui.values[1]));
					var mi = ui.values[0];
					var mx = ui.values[1];
					filterSystem(mi, mx);
				}
			});
			$("#amount").val("$" + agregarPuntos($("#slider-range").slider("values", 0)) + " - $" + agregarPuntos($("#slider-range").slider("values", 1)));
		});

		function filterSystem(minPrice, maxPrice) {
			$("#products li.product").hide().filter(function () {
				var price = parseInt($(this).data("price"), 10);
				return price >= minPrice && price <= maxPrice;
			}).show();
		}

		$('#pills-simulacion-tab').attr('disabled', true);
		$('#pills-simulacion-tab').click(function (evt) {
			$(this).removeAttr("href");
			evt.preventDefault();
			return false;
		});
		$('#pills-autofin-tab').attr('disabled', true);
		$('#pills-autofin-tab').click(function (evt) {
			$(this).removeAttr("href");
			evt.preventDefault();
			return false;
		});

		// Formulario paso a paso

		paso1 = $('#pills-seleccionar, #pills-seleccionar-tab');
		paso2 = $('#pills-simulacion, #pills-simulacion-tab');
		paso3 = $('#pills-autofin, #pills-autofin-tab');

		$('.btn_cotizar').on('click', function () {
			paso1.removeClass('active');
			paso1.removeClass('show');
			paso2.addClass('active');
			paso2.addClass('show');
			paso3.removeClass('active');
			paso3.removeClass('show');
			//$('.paso-2').removeClass('complete');
			//$('.paso-2').addClass('disabled');

			//recogemos variables
			var pmodel = $(this).parent().find('input[name="model"]').val();
			var autofin_model = $(this).parent().find('input[name="autofin_model"]').val();
			var model_number = $(this).parent().find('input[name="model_number"]').val();
			var value_vehicle = $(this).parent().find('input[name="value_vehicle"]').val();
			var brand_vehicle = $(this).parent().find('input[name="brand_vehicle"]').val();
			var value_bono = $(this).parent().find('input[name="value_bono"]').val();
			var value_total = $(this).parent().find('input[name="value_total"]').val();
			var moto_thumb = $(this).parent().find('input[name="moto_thumb"]').val();
			var moto_url = $(this).parent().find('input[name="moto_url"]').val();
			var moto_cat = $(this).parent().find('input[name="moto_cat"]').val();
			console.log("lanzando moto " + pmodel);

			//ponemos variables en lado de producto
			$('#formulario_cotizar input[name="moto_modelo"]').val(pmodel);
			$('#formulario_cotizar input[name="moto_nombre"]').val(pmodel);
			$('#formulario_cotizar input[name="moto_autofin"]').val(autofin_model);
			$('#formulario_cotizar input[name="moto_ano"]').val(model_number);
			$('#formulario_cotizar input[name="moto_marca"]').val(brand_vehicle);
			$('#formulario_cotizar input[name="moto_color"]').val('generic');
			$('#formulario_cotizar input[name="moto_precio"]').val(value_vehicle);
			$('#formulario_cotizar input[name="moto_bono"]').val(value_bono);
			$('#formulario_cotizar input[name="moto_precio_final"]').val(value_total);
			$('#formulario_cotizar input[name="moto_precio_p"]').val(agregarPuntos(value_vehicle));
			$('#formulario_cotizar input[name="moto_bono_p"]').val(agregarPuntos(value_bono));
			$('#formulario_cotizar input[name="moto_precio_final_p"]').val(agregarPuntos(value_total));
			$('#formulario_cotizar input[name="moto_sku"]').val(pmodel);
			$('#formulario_cotizar input[name="moto_url"]').val(moto_url);
			$('#formulario_cotizar input[name="moto_thumb"]').val(moto_thumb);
			$('#formulario_cotizar input[name="moto_cat"]').val(moto_cat);

			$('.thumb_cotizar').attr('src', moto_thumb);
			$('#modelo_cotizar').text(pmodel);
			$('.evaluame').text('Evaluar ' + pmodel + ' >')
		});
		$('#pills-seleccionar-tab').on('click', function () {
			paso1.addClass('active');
			paso1.addClass('show');
			paso2.removeClass('active');
			paso2.removeClass('show');
			paso3.removeClass('active');
			paso3.removeClass('show');
		});
		$('#pills-simulacion-tab').on('click', function () {
			paso1.removeClass('active');
			paso1.removeClass('show');
			paso2.addClass('active');
			paso2.addClass('show');
			paso3.removeClass('active');
			paso3.removeClass('show');
		});
		$('#pills-autofin-tab').on('click', function () {
			paso1.removeClass('active');
			paso1.removeClass('show');
			paso2.removeClass('active');
			paso2.removeClass('show');
			paso3.addClass('active');
			paso3.addClass('show');
		});
		$('.btn-anterior-postventa-paso2').on('click', function () {
			paso3.removeClass('active');
			paso2.addClass('active');
			$('.paso-3').removeClass('complete');
			$('.paso-3').addClass('disabled');
			$('.paso-3').removeClass('active');
		});
		$('.btn-siguiente-postventa-paso2').addClass('disabled');
		$('.evaluame').on('click', function () {
			$("#formulario_cotizar").on('submit', function (evt) {
				//evt.preventDefault();
				// variables para spider en iframe
				var pmodel = $('#formulario_cotizar input[name="moto_autofin"]').val().replace(' ', '+').replace(' ', '+');
				var autofin_model = $('#formulario_cotizar input[name="moto_ano"]').val();
				var brand_vehicle = $('#formulario_cotizar input[name="moto_marca"]').val();
				var value_total = $('#formulario_cotizar input[name="moto_precio_final"]').val();
				var rut = $('#formulario_cotizar input[name="contacto_rut"]').val().replace('.', '').replace('.', '');
				var email = $('#formulario_cotizar input[name="contacto_email"]').val();
				var fono = $('#formulario_cotizar input[name="contacto_telefono"]').val();
				var ces = $('#formulario_cotizar input[name="ces_autofin"]').val();

				var linkAutofin = 'https://www.trinidadautofin.com/SpiderDercoMoto/index.html?idTercero=2&plazo=24&modelo=' + pmodel + '&marca=' + brand_vehicle + '&valorVehiculo=' + value_total + '&ano=' + autofin_model + '&r=' + rut + '&t=' + fono + '&d=' + ces + '&c=9&e=' + email;
				//var linkAutofin = 'https://www.trinidadautofin.com/spider_motos/index.html?idTercero=2&modelo=' + pmodel + '&marca=' + brand_vehicle + '&valorVehiculo=' + value_total + '&ano=' + autofin_model + '&r=' + rut + '&t=' + fono + '&d=' + ces + '&c=9&e=' + email;

				$('#cajaAutofin').attr('src', linkAutofin);
				paso1.removeClass('active');
				paso1.removeClass('show');
				paso2.removeClass('active');
				paso2.removeClass('show');
				paso3.addClass('active');
				paso3.addClass('show');
				$("#formulario_cotizar").show();
			});
		});
	}

	/* 
	------------------------------------------------------------------
		Pagina de Catálogo
	------------------------------------------------------------------
	*/

	if ((window.location.pathname.indexOf("modelos") > -1) || (window.location.pathname == "/modelos/")) {
		//if ((window.location.pathname.indexOf("modelos") > -1) || (window.location.pathname.indexOf("producto") > -1)) {
		//console.log((window.location.pathname == "/modelos/"));

		dataLayer.push({
			'event': 'catalogo',
			'fuente': sbjs.get.current.src,
			'medio': sbjs.get.current.mdm,
			'campaign': sbjs.get.current.cmp,
			'content': sbjs.get.current.cnt,
			'terminos': sbjs.get.current.trm,
			'codigo': sbjs.get.promo.code,
			'gadsSce': localStorage.getItem('gadsSce'),
			'gadsAd': localStorage.getItem('gadsAd'),
			'gadsIdAd': localStorage.getItem('gadsIdAd'),
			'gadsNet': localStorage.getItem('gadsNet')
		});

		$('.cotizar').click(function () {
			$('.caja_opcion_financiar').fadeOut();
			//recogemos variables
			var pmodel = $(this).parent().find('input[name="model"]').val(),
				product_id = $(this).parent().find('input[name="product_id"]').val(),
				use_autofin = $(this).parent().find('input[name="use_autofin"]').val(),
				autofin_model = $(this).parent().find('input[name="autofin_model"]').val(),
				model_number = $(this).parent().find('input[name="model_number"]').val(),
				value_vehicle = $(this).parent().find('input[name="value_vehicle"]').val(),
				brand_vehicle = $(this).parent().find('input[name="brand_vehicle"]').val(),
				value_bono = $(this).parent().find('input[name="value_bono"]').val(),
				value_total = $(this).parent().find('input[name="value_total"]').val(),
				moto_thumb = $(this).parent().find('input[name="moto_thumb"]').val(),
				moto_url = $(this).parent().find('input[name="moto_url"]').val(),
				moto_cat = $(this).parent().find('input[name="moto_cat"]').val();
			console.log("lanzando moto " + pmodel);
			console.log("lanzando moto " + moto_thumb);

			dataLayer.push({
				'event': 'inicioCotizar',
				'modelo': pmodel,
				'priductId': product_id,
				'categoria': moto_cat,
				'precio': value_total,
				'bono': value_bono,
				'precioSinBono': value_vehicle,
				'color': $('#formulario_cotizar input[name="moto_color"]').val(),
				'fuente': $('#formulario_cotizar input[name="utm_source"]').val(),
				'medio': $('#formulario_cotizar input[name="utm_medium"]').val(),
				'campaign': $('#formulario_cotizar input[name="utm_campaign"]').val(),
				'content': $('#formulario_cotizar input[name="utm_content"]').val(),
				'terminos': $('#formulario_cotizar input[name="utm_term"]').val(),
				'codigo': $('#formulario_cotizar input[name="utm_code"]').val(),
				'gadsSce': localStorage.getItem('gadsSce'),
				'gadsAd': localStorage.getItem('gadsAd'),
				'gadsIdAd': localStorage.getItem('gadsIdAd'),
				'gadsNet': localStorage.getItem('gadsNet')
			});

			//ponemos variables en lado de producto
			$('.autofin-container input[name="model"]').val(pmodel);
			$('.autofin-container input[name="use_autofin"]').val(use_autofin);
			$('.autofin-container input[name="autofin_model"]').val(autofin_model);
			$('.autofin-container input[name="model_number"]').val(model_number);
			$('.autofin-container input[name="value_vehicle"]').val(value_vehicle);
			$('.autofin-container input[name="brand_vehicle"]').val(brand_vehicle);
			$('.autofin-container input[name="value_bono"]').val(value_bono);
			$('.autofin-container input[name="value_total"]').val(value_total);
			$('.thumb_cotizar').attr('src', moto_thumb);

			//ponemos variables en formulario
			$('#formulario_cotizar input[name="moto_modelo"]').val(pmodel);
			$('#formulario_cotizar input[name="moto_autofin"]').val(autofin_model);
			$('#formulario_cotizar input[name="moto_ano"]').val(model_number);
			$('#formulario_cotizar input[name="moto_precio"]').val(value_vehicle);
			$('#formulario_cotizar input[name="moto_marca"]').val(brand_vehicle);
			$('#formulario_cotizar input[name="moto_bono"]').val(value_bono);
			$('#formulario_cotizar input[name="moto_precio_final"]').val(value_total);
			$('#formulario_cotizar input[name="moto_sku"]').val(pmodel);
			$('#formulario_cotizar input[name="moto_url"]').val(moto_url);
			$('#formulario_cotizar input[name="moto_thumb"]').val(moto_thumb);
			$('#formulario_cotizar input[name="moto_cat"]').val(moto_cat);

			/* if (autofin_model != '0') {
				$('#formulario_cotizar input:radio[name="financiamiento"][value="evaluar"]').prop('disabled', false);
				$('#formulario_cotizar input:radio[name="financiamiento"][value="cotizar"]').prop('disabled', false);
			} else {
				$('#formulario_cotizar input:radio[name="financiamiento"][value="evaluar"]').prop('disabled', true);
				$('#formulario_cotizar input:radio[name="financiamiento"][value="cotizar"]').prop('disabled', false);
			} */

			$('#formulario_cotizar input[name="moto_precio_p"]').val(agregarPuntos($('#formulario_cotizar input[name="moto_precio"]').val()));
			$('#formulario_cotizar input[name="moto_bono_p"]').val(agregarPuntos($('#formulario_cotizar input[name="moto_bono"]').val()));
			$('#formulario_cotizar input[name="moto_precio_final_p"]').val(agregarPuntos($('#formulario_cotizar input[name="moto_precio_final"]').val()));

			$('h4.name_moto').text(pmodel);
			if (value_bono > 0) {
				$('.bono h4 span').text(agregarPuntos(value_bono));
				$('.bono small span').text('$ ' + agregarPuntos(value_vehicle));
				$('.bono').fadeIn();
			} else {
				$('.bono small span, .bono h4 span').text('');
				$('.bono').fadeOut();
			}
			$('.normal h2 span').text('$ ' + agregarPuntos(value_total));

			//ponemos precios normal, bono, final si aplican
			//var usar_autofin = $('.cotizar_moto input["name=use_autofin"]').val();
			if (use_autofin == "si") {
				console.log("moto use_autofin SI");
				//$('.caja_opcion_financiar').fadeIn();
				//$('input:radio[name="financiamiento"][value="evaluar"]').attr('checked', true);
				//$('#boton_enviar').text(' Cotizar ' + pmodel + ' y Solicitar evaluación > ');
				$("#cuota_cae").val(function () {
					var t;
					return t = $(".autofin-container"),
						t.each(function (t) {
							var e, i, n, o, r;
							o = this,
								e = $(o).find("input[name=autofin_model]").val(),
								r = $(o).find("input[name=value_total]").val(),
								n = $(o).find("input[name=value_vehicle]").val(),
								i = {
									model: e,
									brand: $(o).find("input[name=brand_vehicle]").val(),
									price: r || n,
									//year: (new Date).getFullYear()
									year: $(o).find("input[name=model_number]").val()
								},
								$.ajax({
									url: "https://autofin.dercomotos.cl/api/evaluate",
									method: "POST",
									dataType: "json",
									data: i,
									success: function (t) {
										console.log(t),
											t.data.value && ($(o).removeClass("hidden"),
												$(o).find(".price").text("$ " + t.data.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")),
												$(o).find(".cae").text("36 Cuotas (CAE " + t.data.cae + "%)"))
										//$(o).find(".cae").text("Valor cuota (CAE " + t.data.cae + "%)"))
									},
									error: function (t, e) {
										console.error(e)
									}
								})
						})
				});
			} else if (use_autofin == "no") {
				console.log("moto use_autofin NO");
				//$('.caja_opcion_financiar').fadeOut();
				$('input:radio[name="financiamiento"][value="cotizar"]').attr('checked', true);
				$('#boton_enviar').text(' Cotizar ' + pmodel + ' > ');
				$('.autofin-container').addClass('hidden');
			}

		});
	}


	/* 
	------------------------------------------------------------------
		Pagina de Producto
	------------------------------------------------------------------
	*/
	if ($('body.single-product').length) { //&& $('#moto_autofin').length
		//console.log("pagina de producto");
		$("#moto_autofin").val(function () {
			var t;
			return t = $(".autofin-container"),
				t.each(function (t) {
					var e, i, n, o, r;
					o = this,
						e = $(o).find("input[name=autofin_model]").val(),
						r = $(o).find("input[name=value_total]").val(),
						n = $(o).find("input[name=value_vehicle]").val(),
						i = {
							model: e,
							brand: $(o).find("input[name=brand_vehicle]").val(),
							price: r || n,
							//year: (new Date).getFullYear()
							year: $(o).find("input[name=model_number]").val()
						},
						$.ajax({
							url: "https://autofin.dercomotos.cl/api/evaluate",
							method: "POST",
							dataType: "json",
							data: i,
							success: function (t) {
								console.log("moto con autofin");
								console.log(t),
									t.data.value && ($(o).removeClass("hidden"),
										$(o).find(".price").text("$ " + t.data.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")),
										$(o).find(".cae").text("36 Cuotas (CAE " + t.data.cae + "%)"))
								//$(o).find(".cae").text("Valor cuota (CAE " + t.data.cae + "%)"))
							},
							error: function (t, e) {
								console.log("moto sin autofin");
								console.error(e)
							}
						})
				})
		});

		var color_moto = $('.variations_form .variations .soopas-swatches .swatch.selected').data('value');
		if ($('.variations_form .variations .soopas-swatches .swatch.selected').data('value') == undefined) {
			$('#formulario_cotizar input[name="moto_color"]').val('generic');
		} else {
			$('#formulario_cotizar input[name="moto_color"]').val(color_moto);
		}

		$(".soopas-swatches .swatch-color").on('click', function () {
			console.log("click cambio de color");

			console.log("cambiando de color");
			setTimeout(
				function () {
					var precio_normal_1 = $('.single_variation_wrap del .woocommerce-Price-amount').text().replace('.', '').replace('.', '').replace('$', '');
					var precio_oferta_1 = $('.single_variation_wrap ins .woocommerce-Price-amount').text().replace('.', '').replace('.', '').replace('$', '');
					var color_moto = $('.variations_form .variations .soopas-swatches .swatch.selected').data('value');
					$('#formulario_cotizar input[name="moto_color"]').val(color_moto);

					var valor_bono_1 = precio_normal_1 - precio_oferta_1;

					if (valor_bono_1 > 0) {
						console.log("color con bono");
						//alert('precio oferta: '+precio_oferta+' | precio normal: '+precio_normal+' | bono: '+valor_bono );
						$('.autofin-container input[name="value_vehicle"]').val(precio_normal_1);
						$('.autofin-container input[name="value_bono"]').val(valor_bono_1);
						$('.autofin-container input[name="value_total"]').val(precio_oferta_1);

						$('#formulario_cotizar input[name="moto_precio"]').val(precio_normal_1);
						$('#formulario_cotizar input[name="moto_bono"]').val(valor_bono_1);
						$('#formulario_cotizar input[name="moto_precio_final"]').val(precio_oferta_1);

						$('#moto_autofin .bono h4').text('Incluye bono: $ ' + agregarPuntos(valor_bono_1));
						$('#moto_autofin .bono small span').text('$ ' + agregarPuntos(precio_normal_1));
						$('#moto_autofin .bono').fadeIn();
						$('#moto_autofin .normal h2').text('$ ' + agregarPuntos(precio_oferta_1));

						$('.bk-cot--card__content--price .bono h4').text('Incluye bono: $ ' + agregarPuntos(valor_bono_1));
						$('.bk-cot--card__content--price .bono small span').text('$ ' + agregarPuntos(precio_normal_1));
						$('.bk-cot--card__content--price .bono').fadeIn();
						$('.bk-cot--card__content--price .normal h2').text('$ ' + agregarPuntos(precio_oferta_1));

						$('.head-valfinal h2').text('$ ' + agregarPuntos(precio_oferta_1));
						$('.head-valnormal small span').text('$ ' + agregarPuntos(precio_normal_1));
						$('.head-valbono small span').text('$ ' + agregarPuntos(valor_bono_1));
						$('.head-valnormal').fadeIn();
						$('.head-valbono').fadeIn();
					} else {
						console.log("color sin bono");
						//alert('precio normal: ' + precio_normal);
						$('.autofin-container input[name="value_vehicle"]').val(precio_normal_1);
						$('.autofin-container input[name="value_bono"]').val(valor_bono_1);
						$('.autofin-container input[name="value_total"]').val(precio_normal_1);

						$('#formulario_cotizar input[name="moto_precio"]').val();
						$('#formulario_cotizar input[name="moto_bono"]').val();
						$('#formulario_cotizar input[name="moto_precio_final"]').val(precio_normal_1);

						$('.bono small span, .bono h4 span').text('');
						$('.bono').fadeOut();
						$('.normal h2 span').text('$ ' + agregarPuntos(precio_normal_1));

						$('.head-valfinal h2').text('$ ' + agregarPuntos(precio_oferta_1));
						$('.head-valnormal small span').text('$ ' + agregarPuntos(precio_normal_1));
						$('.head-valbono small span').text('$ ' + agregarPuntos(valor_bono_1));
						$('.head-valnormal').fadeOut();
						$('.head-valbono').fadeOut();
					}
					$('#formulario_cotizar input[name="moto_precio_p"]').val(agregarPuntos($('#formulario_cotizar input[name="moto_precio"]').val()));
					$('#formulario_cotizar input[name="moto_bono_p"]').val(agregarPuntos($('#formulario_cotizar input[name="moto_bono"]').val()));
					$('#formulario_cotizar input[name="moto_precio_final_p"]').val(agregarPuntos($('#formulario_cotizar input[name="moto_precio_final"]').val()));

					console.log("actualizando autofin");
					$("#moto_autofin").val(function () {
						var t;
						return t = $(".autofin-container"),
							t.each(function (t) {
								var e, i, n, o, r;
								o = this,
									e = $(o).find("input[name=autofin_model]").val(),
									r = $(o).find("input[name=value_total]").val(),
									n = $(o).find("input[name=value_vehicle]").val(),
									i = {
										model: e,
										brand: $(o).find("input[name=brand_vehicle]").val(),
										price: r || n,
										//year: (new Date).getFullYear()
										year: $(o).find("input[name=model_number]").val()
									},
									$.ajax({
										url: "https://autofin.dercomotos.cl/api/evaluate",
										method: "POST",
										dataType: "json",
										data: i,
										success: function (t) {
											console.log("moto con autofin");
											console.log(t),
												t.data.value && ($(o).removeClass("hidden"),
													$(o).find(".price").text("$ " + t.data.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")),
													$(o).find(".cae").text("36 Cuotas (CAE " + t.data.cae + "%)"))
											//$(o).find(".cae").text("Valor cuota (CAE " + t.data.cae + "%)"))
										},
										error: function (t, e) {
											console.log("moto sin autofin");
											console.error(e)
										}
									})
							})
					});
				},
				300);
			console.log("color completado");
		});

		$('.bk-cotizar--producto').click(function () {
			//recogemos variables
			var scope = $('#formulario_cotizar'),
				pmodel = $(scope).find('input[name="moto_modelo"]').val(),
				product_id = $('input[name="product_id"]').val(),
				value_vehicle = $(scope).find('input[name="moto_precio"]').val(),
				value_bono = $(scope).find('input[name="moto_bono"]').val(),
				value_total = $(scope).find('input[name="moto_precio_final"]').val(),
				moto_cat = $(scope).find('input[name="moto_cat"]').val();
			console.log("lanzando moto " + pmodel);

			if ($('input[name="variation_id"]').val()) {
				product_id = $('.woocommerce-variation-add-to-cart').find('input[name="variation_id"]').val();
			}

			dataLayer.push({
				'event': 'inicioCotizar',
				'procutId': product_id,
				'modelo': pmodel,
				'categoria': moto_cat,
				'precio': value_total,
				'bono': value_bono,
				'precioSinBono': value_vehicle,
				'color': $('#formulario_cotizar input[name="moto_color"]').val(),
				'fuente': $('#formulario_cotizar input[name="utm_source"]').val(),
				'medio': $('#formulario_cotizar input[name="utm_medium"]').val(),
				'campaign': $('#formulario_cotizar input[name="utm_campaign"]').val(),
				'content': $('#formulario_cotizar input[name="utm_content"]').val(),
				'terminos': $('#formulario_cotizar input[name="utm_term"]').val(),
				'codigo': $('#formulario_cotizar input[name="utm_code"]').val(),
				'gadsSce': localStorage.getItem('gadsSce'),
				'gadsAd': localStorage.getItem('gadsAd'),
				'gadsIdAd': localStorage.getItem('gadsIdAd'),
				'gadsNet': localStorage.getItem('gadsNet')
			});

		});

		dataLayer.push({
			'event': 'verProducto',
			'procutId': $('input[name="product_id"]').val(),
			'modelo': $('#formulario_cotizar input[name="moto_modelo"]').val(),
			'categoria': $('#formulario_cotizar input[name="moto_cat"]').val(),
			'precio': $('#formulario_cotizar input[name="moto_precio_final"]').val(),
			'bono': $('#formulario_cotizar input[name="moto_bono"]').val(),
			'precioSinBono': $('#formulario_cotizar input[name="moto_precio"]').val(),
			'color': $('#formulario_cotizar input[name="moto_color"]').val(),
			'fuente': $('#formulario_cotizar input[name="utm_source"]').val(),
			'medio': $('#formulario_cotizar input[name="utm_medium"]').val(),
			'campaign': $('#formulario_cotizar input[name="utm_campaign"]').val(),
			'content': $('#formulario_cotizar input[name="utm_content"]').val(),
			'terminos': $('#formulario_cotizar input[name="utm_term"]').val(),
			'codigo': $('#formulario_cotizar input[name="utm_code"]').val(),
			'gadsSce': localStorage.getItem('gadsSce'),
			'gadsAd': localStorage.getItem('gadsAd'),
			'gadsIdAd': localStorage.getItem('gadsIdAd'),
			'gadsNet': localStorage.getItem('gadsNet')
		});

		var videoTitle = $('.bk-video__title');
		videoTitle.html(videoTitle.text().replace(/(\w+\s\w+?)$/, '<span class="bk--title__i">$1</span>'));

	}
	/* 
	------------------------------------------------------------------
		Slide de productos
	------------------------------------------------------------------
	*/
	function hermanos() {
		//Checkear si existe la clase de los clones
		if ($('.bk-siblings')[0]) {} else {
			//Por cada item de producto
			$('.bk-loop--productos .carousel .carousel-item').each(function () {
				// Agregar clases para maquetar
				$(this).addClass('row no-gutters');
				$('.carousel-wrapper').addClass('col-4');

				// Seleccionar el siguiente item
				var next = $(this).next();
				//seleccionar el primer item ya que el último no tiene siguiente
				if (!next.length) {
					next = $(this).siblings(':first');
				}

				// Clonar productos
				next.children(':first-child').clone().removeClass('bk-siblings').appendTo($(this));
				// Condición al clonar
				if (next.next().length > 0) {
					next.next().children(':first-child').clone().appendTo($(this));
				} else {
					$(this).siblings(':first').children(':first-child').clone().appendTo($(this));
				}

				// Agregar clase al original	
				$('.carousel-wrapper:nth-child(2)').addClass('bk-one');
				// Agregar clase a los clones
				$('.bk-one').siblings().addClass('bk-siblings');
			});
		}
	}

	function destroyHermanos() {
		// Si existen los clones eliminarlos y remover clases de maquetación
		if ($('.bk-siblings')[0]) {
			$('.home .bk-loop--productos .carousel .carousel-item').each(function () {
				$('.bk-siblings').remove();
				$(this).removeClass('.row .no-gutters');
				$('.carousel-wrapper').removeClass('col-4 bk-one');
			});
		}
	}
	//Ejecutar funcion si la ventana es mayor a 780px
	if ($(window).width() >= 780) {
		hermanos();
	}
	// Ejecutar función al modificar el tamaño de la ventana  
	$(window).resize(function () {
		if ($(window).width() >= 780) {
			hermanos();
		} else {
			destroyHermanos();
		}
	});

	/* 
	------------------------------------------------------------------
		Fixed boton al hacer scroll
	------------------------------------------------------------------
	*/
	$(window).scroll(function () {
		if ($(window).scrollTop() > 300) {
			$('.bk-fixed-nav').addClass('bk-show');

		} else {
			$('.bk-fixed-nav').removeClass('bk-show');
		}
	});

	/*
	------------------------------------------------------------------
		360
	------------------------------------------------------------------
	*/
	$('.bk-360-content').hide();

	$('#bk-360-btn').click(function (e) {
		e.preventDefault();
		if ($(".bk-360-content").is(":hidden")) {
			$('#hotspotImg').css('display', 'none');
			$(".bk-360-content").show();

		} else {
			$(".bk-360-content").hide();
			$('#hotspotImg').show();
		}
		//$('.bk-360-content').show();
	});
	/*
	------------------------------------------------------------------
		Formulario de inscripción footer
	------------------------------------------------------------------
	*/
	$(".bk-mailing-visible--input").keyup(function () {
		var value = $(this).val();
		$("#input2").val(value);
	});

	$('#bkSbmitFooterBtn').on('click', function () {
		$('.bk-mailing--submit').trigger("click");
	});

	$(".bk-home--search__form .bk-search--input").keyup(function () {
		$('#searchsubmit').attr('disabled', false);
	});
	/*
	------------------------------------------------------------------
		Experience
	------------------------------------------------------------------
	*/
	$('.bk-home--events__cards:first-child').removeClass('col-sm-4').addClass('col-sm-12 bk-home--events__first');
	/*
	------------------------------------------------------------------
		Parametros de url
	------------------------------------------------------------------
	*/
	function getParameterByName(name, url) {
		if (!url) url = window.location.href;
		name = name.replace(/[\[\]]/g, "\\$&");
		var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
			results = regex.exec(url);
		if (!results) return null;
		if (!results[2]) return '';
		return decodeURIComponent(results[2].replace(/\+/g, " "));
	}
	var avProjectName = getParameterByName('m');
	if (!avProjectName == '') {
		$('.bk-woocommerce').find('.product.status-publish').hide();
		$('.product.product_cat-' + avProjectName).show();
		$('.bk-wc-category--list__item').removeClass('active');
	}

	$('#js_selector_cat_mb').change(function () {
		var catMb = $(this).val();
		acUrl = window.location.protocol + "//" + window.location.host + window.location.pathname;
		$('.bk-woocommerce').find('.product.status-publish').hide();
		$('.product.product_cat-' + catMb).show();
		$('.bk-wc-category--list__item').removeClass('active');
		if (catMb == 'todas') {
			$('.bk-woocommerce').find('.product.status-publish').show();
		}
	});


	// Mensajes Personalizados
	jQuery.extend(jQuery.validator.messages, {
		required: "Este campo es obligatorio",
		remote: "Please fix this field.",
		email: "Por favor ingresa un correo válido.",
		url: "Please enter a valid URL.",
		date: "Please enter a valid date.",
		dateISO: "Please enter a valid date (ISO).",
		number: "Please enter a valid number.",
		lettersonly: "Por favor ingresa sólo letras.",
		digits: "Por favor ingresa sólo números.",
		creditcard: "Please enter a valid credit card number.",
		equalTo: "Please enter the same value again.",
		accept: "Please enter a value with a valid extension.",
		maxlength: jQuery.validator.format("Por favor ingresa un teléfono de 9 digitos."),
		minlength: jQuery.validator.format("Por favor ingresa un teléfono de 9 digitos."),
		rangelength: jQuery.validator.format("Please enter a value between {0} and {1} characters long."),
		range: jQuery.validator.format("Please enter a value between {0} and {1}."),
		max: jQuery.validator.format("Por favor ingresa un teléfono de 9 digitos."),
		min: jQuery.validator.format("Por favor ingresa un teléfono de 9 digitos.")
	});


	$('.experience-future--child').each(function (i, el) {
		if (!(i % 2 === 0)) {
			$(this).find('.bk-home--events__cards--row').addClass('experience-odd flex-row-reverse');
		}
	});
	//$('.experience-odd .bk-home--events__cards--img').remove().insertAfter($('.bk-home--events__cards--text'));


	$('.bk-product--slider').owlCarousel({
		autoplay: true,
		autoplayTimeout: 2500,
		slideTransition: 'ease-in-out',
		loop: true,
		margin: 10,
		items: 1,
		lazyLoad: true,
		responsive: {
			0: {
				items: 1
			},
			600: {
				stagePadding: 100
			},
			700: {
				stagePadding: 160
			},
			800: {
				stagePadding: 220
			},
			1000: {
				stagePadding: 300
			},
			1200: {
				stagePadding: 400
			},
			1400: {
				stagePadding: 480
			},
			1500: {
				stagePadding: 500
			},
			1600: {
				stagePadding: 510
			},
			1700: {
				stagePadding: 640
			},
		}
	});
	$('.bk-novedades--slider').owlCarousel({
		autoplay: true,
		autoplayTimeout: 2500,
		loop: true,
		items: 1,
		lazyLoad: true,
	});

	$('.bk-high--container').owlCarousel({
		autoplay: false,
		autoplayTimeout: 2500,
		center: true,
		loop: false,
		dots: false,
		onInitialized: callback,
	});

	function callback(event) {
		var items = event.item.count;
		console.log(items);
		console.log(items < 2);
		if (items == 1) {
			$('.bk-high--container').owlCarousel({
				items: 1,
			});
		}else if (items == 2) {
			$('.bk-high--container').owlCarousel({
				responsive: {
					0: {
						items: 1
					},
					400: {
						items: 2
					}
				}
			});
		}else{
			$('.bk-high--container').owlCarousel({
				loop:true,
				responsive: {
					0: {
						items: 1
					},
					400: {
						items: 3
					}
				}
			});
		}
	}

	$(window).on("load", function () {
		var dataCae = $('.bk-shop--banner__txt').find('.cae').text();
		$('.valor-cae .cae').text(dataCae);
	});
	//hide and show cotizar bar
	$('#btn-hide').on('click', function(e) {
		$('.active-info').toggleClass("hide-detail");
		e.preventDefault();
	});

	//slider home
	$('#banner-top').owlCarousel({
		autoplay: true,
		autoplayTimeout: 3000,
		loop: true,
		items: 1,
		lazyLoad: true,
	});
	/* var owl = $('.bk-product--slider');
	owl.owlCarousel();
	// Listen to owl events:
	owl.on('changed.owl.carousel', function (event) {
		$('.owl-item.active').each(function (){
			$('.owl-item.active').css('opacity', 1);
			$('.owl-item.active:nth-child(odd)').css('opacity', .5);
			console.log('funca');
		}); 
	})*/
	/*
	------------------------------------------------------------------
		Fin del document.ready
	------------------------------------------------------------------
	*/
});

// Desactiva múltiples envíos
jQuery(document).on('click', '.wpcf7-submit', function (e) {
	if (jQuery('.ajax-loader').hasClass('is-active')) {
		e.preventDefault();
		return false;
	}
});
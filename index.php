<?php get_template_part('includes/header'); ?>

<section class="container mt-5">
  <div class="row">

    <div class="col-sm">
      <div id="content" role="main">

        <?php get_template_part('includes/loops/index-loop'); ?>

      </div><!-- /#content -->
    </div>

  </div><!-- /.row -->
</section><!-- /.container -->

<?php get_template_part('includes/footer'); ?>

<?php get_template_part('includes/header'); ?>
<img src="<?php bloginfo('template_directory')?>/assets/img/experience_hero_1.jpg" alt="" style="width:100%;">
<?php 
$today = date("Ymd");
?>
  <section class="container-fluid bk-home--events">
    <div class="container">
      <div class="row mt-5 mb-5">
      <!-- <h3 class="text-center w-100 pb-5 bk-title--red">Proximos</h3> -->
      <?php 
      $date_args = array(
        'post_type'              => 'experience',
        'posts_per_page' => 4,
        'meta_query'             => array(
            array(
                'key'       => 'ex_fecha_del_evento',
                'value'     => $today,
                'compare'   => '>=',
                'type'      => 'DATE',
            ),
        ),
        'meta_key'               => 'ex_fecha_del_evento',
        'orderby'                => 'meta_value',
        'order'                  => 'ASC'
      );
      $cat_3 = new WP_Query( $date_args ); 
      ?>

      <?php if ( $cat_3->have_posts() ) :?>
        <div class="container">
          <div class="row justify-content-between bk-home--events--nav">
            <div class="col-auto d-flex align-self-stretch">
              <h2 class="bk-home--events--nav__title">PROXIMOS EVENTOS</h2>
            </div>
            <div class="col-auto d-flex align-self-stretch bk-home--events--nav__link">
              <a href="#" class="align-self-center bk-home--events--nav__link-btn"><i class="fas fa-plus-circle"></i></a>
            </div>
          </div>
        </div>

        <div class="container bk-home--events__cards--container">
          <div class="row bk-home--events__cards--row">
            <?php while ($cat_3->have_posts()) : $cat_3->the_post(); ?>
              <div class="col-sm-4 bk-home--events__cards">
                <article role="article" id="post_<?php the_ID()?>" <?php post_class("bk-loop-card"); ?> >
                  <header class="bk-loop-card--header">
                    <a href="<?php the_permalink(); ?>">
                      <?php the_post_thumbnail('medium', ['class'=>'w-100']); ?>
                    </a>
                  </header>
                  <div class="bk-loop-card--content">
                    <h4>
                      <a href="<?php the_permalink(); ?>">
                        <?php the_title()?>
                      </a>
                    </h4>
                  <a href="<?php the_permalink(); ?>" class="bk--btn bk--btn__black bk--btn__small">ver más </a>
                  </div>
                </article>
              </div>
          <?php endwhile; 
          wp_reset_postdata(); 
          endif;?>

          </div>
        </div>

      </div>
    </div>
  </section>

  
  <section class="container-fluid pb-5 pt-5" style="background:#f4f4f4;">
    <div class="container">
      <div class="row mt-5 mb-5">
      <!-- <h3 class="text-center w-100 pb-5 bk-title--red">Proximos</h3> -->
      <?php 
      $date_args = array(
        'post_type'              => 'experience',
        'posts_per_page' => -1,
        'meta_query'             => array(
            array(
                'key'       => 'ex_fecha_del_evento',
                'value'     => $today,
                'compare'   => '<=',
                'type'      => 'DATE',
            ),
        ),
        'meta_key'               => 'ex_fecha_del_evento',
        'orderby'                => 'meta_value',
        'order'                  => 'ASC'
      );
      $cat_3 = new WP_Query( $date_args ); 
      ?>

      <?php if ( $cat_3->have_posts() ) :?>
        <div class="container">
        <div class="bk--title mb-4">
                <h2 class="text-center">Eventos<span class="bk--title__i"> realizados</span></h2>
                <p class="text-center">- WAY OF LIFE -</p>
            </div>
        </div>
        <div class="container bk-home--events__cards--container">
          <div class="row bk-home--events__cards--row">
            <?php while ($cat_3->have_posts()) : $cat_3->the_post(); ?>
              <div class="col-sm-4">
                <article role="article" id="post_<?php the_ID()?>" <?php post_class("bk-loop-card"); ?> >
                  <header class="bk-loop-card--header">
                    <a href="<?php the_permalink(); ?>">
                      <?php the_post_thumbnail('medium', ['class'=>'w-100']); ?>
                    </a>
                  </header>
                  <div class="bk-loop-card--content">
                    <h4>
                      <a href="<?php the_permalink(); ?>">
                        <?php the_title()?>
                      </a>
                    </h4>
                  </div>
                </article>
              </div>
          <?php endwhile; 
          wp_reset_postdata(); 
          endif;?>

          </div>
        </div>

      </div>
    </div>
  </section>

<?php get_template_part('includes/footer'); ?>

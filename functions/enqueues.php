<?php

/**!
 * Enqueues
 */
$url = 'https://code.jquery.com/jquery-latest.min.js';
$test_url = @fopen($url, 'r');
if ($test_url !== false) {
	function load_external_jQuery()
	{
		wp_deregister_script('jquery');
		wp_register_script('jquery', 'https://code.jquery.com/jquery-latest.min.js');
		wp_enqueue_script('jquery');
	}
	add_action('wp_enqueue_scripts', 'load_external_jQuery');
} else {
	function load_local_jQuery()
	{
		wp_deregister_script('jquery');
		wp_register_script('jquery', get_bloginfo('template_url') . './assets/js/jquery.min.js', __FILE__, false, '1.11.3', true);
		wp_enqueue_script('jquery');
	}
	add_action('wp_enqueue_scripts', 'load_local_jQuery');
}


if (!function_exists('b4st_enqueues')) {
	function b4st_enqueues()
	{

		// Styles

		wp_register_style('bootstrap-css', 'https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.min.css', false, '4.1.1', null);
		wp_enqueue_style('bootstrap-css');

		//wp_register_style('b4st-css', get_template_directory_uri() . '/assets/css/b4st.css', false, null);
		//wp_enqueue_style('b4st-css');

		// Scripts

		wp_register_script('font-awesome-config-js', get_template_directory_uri() . '/assets/js/font-awesome-config.js', false, null, null);
		wp_enqueue_script('font-awesome-config-js');

		wp_register_script('font-awesome', 'https://use.fontawesome.com/releases/v5.0.13/js/all.js', false, '5.0.13', null);
		wp_enqueue_script('font-awesome');

		/* wp_register_script('modernizr',  'https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js', false, '2.8.3', true);
		wp_enqueue_script('modernizr'); */

		wp_register_script('popper', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js', false, '1.14.3', true);
		wp_enqueue_script('popper');

		wp_register_script('bootstrap-js', 'https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/js/bootstrap.min.js', false, '4.1.1', true);
		wp_enqueue_script('bootstrap-js');

		//Tracking de ADS
		wp_register_script('sourcebuster-js', get_template_directory_uri() . '/assets/js/sourcebuster.min.js', false, '1.1.1', true);
		wp_enqueue_script('sourcebuster-js');

		if(is_page( 'Financiamiento' )) {
			
			wp_register_script('ui-js', 'https://code.jquery.com/ui/1.12.1/jquery-ui.js', false, '1.12.1', true);
			wp_enqueue_script('ui-js');

			wp_register_style('ui-css', 'https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css');
			wp_enqueue_style('ui-css');

			wp_register_style('demo-css', 'https://jqueryui.com/resources/demos/style.css');
			wp_enqueue_style('demo-css');
        }

		//validaciones rut
		wp_register_script('validate-js', get_template_directory_uri() . '/assets/js/jquery.validate.min.js', false, null, true);
		wp_enqueue_script('validate-js');
		wp_register_script('valid-method-js', get_template_directory_uri() . '/assets/js/additional-methods.min.js', false, null, true);
		wp_enqueue_script('valid-method-js');
		wp_register_script('rut-js', get_template_directory_uri() . '/assets/js/rut.js', false, null, true);
		wp_enqueue_script('rut-js');

		//wp_register_script('b4st-js', get_template_directory_uri() . '/assets/js/b4st.js', false, null, true);
		//wp_enqueue_script('b4st-js');

		if (is_singular() && comments_open() && get_option('thread_comments')) {
			wp_enqueue_script('comment-reply');
		}

	}
}
add_action('wp_enqueue_scripts', 'b4st_enqueues', 100);

function boken_enqueues()
{
	if (is_single()) {
		wp_register_style('lightbox-css', 'https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.css');
		wp_enqueue_style('lightbox-css');
		wp_register_script('lightbox-js', 'https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.js');
		wp_enqueue_script('lightbox-js');
	}

	if (is_singular('product')) {
		wp_register_script('hotspot-js', get_stylesheet_directory_uri() . '/assets/js/jquery.hotspot.js', false, null, true);
		wp_enqueue_script('hotspot-js');
		wp_register_script('fullvideo-js', get_template_directory_uri() . '/assets/js/jquery.youtubebackground.js', false, null);
		$script_data = array(
			'video_url' => get_field('enlace_video'),
		);
		wp_localize_script(
			'fullvideo-js',
			'fullvideo',
			$script_data
		);
		wp_enqueue_script('fullvideo-js');
		/* wp_register_style('video-setup-css', get_template_directory_uri() . '/assets/css/main-dist.css', false, null);
		wp_enqueue_style('video-setup-css'); */
		wp_register_script('hotspot-setup-js', get_stylesheet_directory_uri() . '/assets/js/hotspot-setup.js', false, null, true);
		wp_enqueue_script('hotspot-setup-js');
	}

	wp_register_style('boken-css', get_template_directory_uri() . '/assets/css/main-dist.css', false, null);
	wp_enqueue_style('boken-css');

	wp_register_script('boken-js', get_template_directory_uri() . '/assets/js/main-dist.js', false, null, true);
	$script_data = array(
		'template_directory_uri' => get_template_directory_uri(),
		'templateUrl' => home_url(),
		'ajaxurl' => admin_url('admin-ajax.php')
	);
	wp_localize_script(
		'boken-js',
		'my_data',
		$script_data
	);

	wp_enqueue_script('boken-js');

	if (is_post_type_archive('concesionarios')) {
		//wp_enqueue_script('google-api', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDQlENQl0FlrkzVPLPbES58YF_t7ZjywqU', null, null); // Add in your key
		
		wp_register_script('google-map', get_stylesheet_directory_uri() . '/assets/js/concesionarios.js', false, null, true);
		$mapData = array(
			'templateUrl' => get_template_directory_uri()
		);
		wp_localize_script(
			'google-map',
			'my_data',
			$mapData
		);
		wp_enqueue_script('google-map');
	}
}
add_action('wp_enqueue_scripts', 'boken_enqueues', 100);
<?php get_template_part('includes/header'); ?>

<section class="container mt-5">
  <h2 class="text-center bk-title--red">
    <?php _e('Categoría: ', 'b4st'); echo single_cat_title(); ?>
  </h2>
  <hr>
</section>
<section class="container mt-5 mb-5">
        <?php get_template_part('includes/loops/index-loop'); ?>
</section><!-- /.container -->

<?php get_template_part('includes/footer'); ?>

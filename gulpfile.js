const gulp = require('gulp'),
    browserSync = require('browser-sync').create(), //Recarga el navegador
    autoprefixer = require('gulp-autoprefixer'), //Autoprefixer
    notify = require('gulp-notify'), //Notificaciones
    sass = require('gulp-sass'), //Copila SCSS
    sourcemaps = require('gulp-sourcemaps'), //SourceMaps
    //concat = require('gulp-concat'), //Concatena archivos js (hay ubo para css)
    uglify = require('gulp-uglify'), //Minifica JS
    cleanCSS = require('gulp-clean-css'), //Minifica CSS
    rename = require('gulp-rename'), //Renombra archivos
    gutil = require('gulp-util'),
    ftp = require('vinyl-ftp'); //Minifica JS

gulp.task('browser-sync', function () {
    browserSync.init({
        proxy: "http://localhost/boken",
        port: 8080
    });
});

gulp.task('sass', function () {
    return gulp.src('./assets/scss/main.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({
            outputStyle: 'expanded',
            sourceComments: true
        }))
        .on("error", notify.onError({
            sound: true,
            title: 'Error de copilación SCSS'
        }))
        .pipe(autoprefixer({
            versions: ['last 2 browsers']
        }))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('./assets/css/'));
});


gulp.task('minify-css', function () {
    return gulp.src('./assets/css/main.css')
        .pipe(rename({
            suffix: '-dist'
        }))
        .pipe(sourcemaps.init({
            loadMaps: true
        }))
        .pipe(cleanCSS())
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('./assets/css/'))
        .pipe(notify({
            sound: false,
            message: 'Css Procesado',
            onLast: true
        }))
        .pipe(browserSync.stream({
            match: '**/*.css'
        }));
});

gulp.task('minify-js', function () {
    return gulp.src('./assets/js/main.js')
        .pipe(uglify()
            .on("error", notify.onError({
                sound: true,
                title: 'Error en JS'
            })))
        .pipe(rename({
            suffix: '-dist'
        }))
        .pipe(gulp.dest('./assets/js/'))
        .pipe(notify({
            sound: false,
            message: 'Js Procesados',
            onLast: true
        }));
});

gulp.task('deploy', function () {

    var conn = ftp.create({
        host: 'ftp.oqodigital.net',
        user: 'oqodigital',
        password: 'KJ6DqN7Xx6.',
        parallel: 4,
        log: gutil.log
    });

    var globs = [
        './assets/**',
        './functions/**',
        './includes/**',
        './*.png',
        './*.php',
        './*.js',
        './*.css'
    ];

    // using base = '.' will transfer everything to /public_html correctly
    // turn off buffering in gulp.src for best performance

    return gulp.src(globs, {
            base: '.',
            buffer: false
        })
        .pipe(conn.newerOrDifferentSize('/clientes/derco-motos.oqodigital.net/wp-content/themes')) // only upload newer files
        .pipe(conn.dest('/clientes/derco-motos.oqodigital.net/wp-content/themes'));

});

gulp.task('watch', ['browser-sync'], function () {
    gulp.watch('./**/*.scss', ['sass']);
    gulp.watch('./**/*.css', ['minify-css']);
    gulp.watch('./**/*.php').on('change', browserSync.reload);
    gulp.watch('./**/*.js', ['minify-js']).on('change', browserSync.reload);
});

gulp.task('default', ['sass', 'minify-css', 'minify-js']);
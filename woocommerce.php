<?php 
get_template_part('includes/header');

if( is_product()){

  global $product;

  if ($product->is_type('simple')) {
    $precio_normal  = (int)get_post_meta(get_the_ID(), '_regular_price', true);;
    $precio_oferta  = (int)get_post_meta(get_the_ID(), '_sale_price', true);
    if ($precio_oferta) {
        $valor_bono = ($precio_normal-$precio_oferta);
    } else {
        $valor_bono = (int)0;
    }
    $precio_final   = ($precio_normal-$valor_bono);
  }

  if ($product->product_type == 'variable') {
    #Step 1: Get product varations
    $available_variations = $product->get_available_variations();
    #Step 2: Get product variation id
    $variation_id = $available_variations[0]['variation_id']; // Getting the variable id of just the 1st product. You can loop $available_variations to get info about each variation.
    #Step 3: Create the variable product object
    $variable_product1 = new WC_Product_Variation($variation_id);
    #Step 4: You have the data. Have fun :)
    $precio_normal = $variable_product1->regular_price;
    $precio_oferta = $variable_product1->sale_price;
    if ($precio_oferta) {
        $valor_bono = ($precio_normal-$precio_oferta);
    } else {
        $valor_bono = (int)0;
    }
    $precio_final   = ($precio_normal-$valor_bono);
  }

  if (empty($precio_oferta) && empty($precio_oferta)){
    echo '
    <nav class="bk-fixed-nav container-fluid navbar navbar-expand-lg navbar-light bg-light">
      <div class="container">
        <div class="row w-100 d-flex align-items-center">
          <div class="col-6 col-md-3 pright0 d-none d-md-block">
            <button class="cd-nav-trigger bk-woocommerce--shop__btn bk--btn bk--btn__primary bk--btn__small">Cotizar ' . $product->name . ' <i class="fas fa-chevron-right"></i></button>
          </div>
          <div class="col-6 col-md-3 pright0 d-md-none">
            <button class="cd-nav-trigger bk-woocommerce--shop__btn bk--btn bk--btn__primary bk--btn__small">Cotizar <i class="fas fa-chevron-right"></i></button>
          </div>
          <div class="col-6 d-md-none pright0" style="padding-left:0px;">
            <button id="btn-hide" class="bk--btn bk--btn__primary bk--btn__small">Detalle <i class="fas fa-chevron-down"></i></button>
          </div> 
          <div class="pl-3 active-info hide-detail">
            <div class="normal head-valfinal text-center">
              <h2 style="margin:0;">$ ' . number_format($precio_final, 0, ",", ".") . '</h2>
              <div class="valor-cae" style="color:#41b451;">
                <small class="cae" ></small>
                <small class="price"></small>
              </div>
            </div>
          </div>';
  } else {
    echo '
    <nav class="bk-fixed-nav container-fluid navbar navbar-expand-lg navbar-light bg-light">
      <div class="container">
        <div class="row w-100 d-flex align-items-center">
          <div class="col-6 col-md-3 pright0 d-none d-md-block">
            <button class="cd-nav-trigger bk-woocommerce--shop__btn bk--btn bk--btn__primary bk--btn__small">Cotizar ' . $product->name . ' <i class="fas fa-chevron-right"></i></button>
          </div>
          <div class="col-6 col-md-3 pright0 d-md-none">
            <button class="cd-nav-trigger bk-woocommerce--shop__btn bk--btn bk--btn__primary bk--btn__small">Cotizar <i class="fas fa-chevron-right"></i></button>
          </div>
          <div class="col-6 d-md-none pright0" style="padding-left:0px;">
            <button id="btn-hide" class="bk--btn bk--btn__primary bk--btn__small">Detalle <i class="fas fa-chevron-down"></i></button>
          </div> 
          <div class="col-12 col-md-3 active-info hide-detail">
            <div class="normal head-valfinal text-center">
              <h2 style="margin:0;">$ ' . number_format($precio_final, 0, ",", ".") . '</h2>
              <div class="valor-cae" style="color:#41b451;">
                <small class="cae" ></small>
                <small class="price"></small>
              </div>
            </div>
          </div>';
  }
  if (!empty($precio_oferta)) {
    echo '
          <div class="col-6 col-md-3 active-info hide-detail">
            <div class="bono head-valbono">
              <small>Incluye bono:</small>
              <br>
              <small style="color:#41b451; font-weight:900">$ ' . number_format($valor_bono, 0, ",", ".") . '</small>
            </div>
          </div>';
  }
      // si el producto esta en oferta mostramos precio normal
  if (!empty($precio_oferta)) {
    echo '
          <div class="col-6 col-md-3 active-info hide-detail">
            <div class="bono head-valnormal">
                <small>Precio Normal:</small>
                <br>
                <small style="color:#41b451; font-weight:900">$ ' . number_format($precio_normal, 0, ",", ".") . '</small>
            </div>
          </div>
            ';
  }
  echo '
      </div>
    </div>
  </nav>
  ';
}
?>

<div class="bk-woocommerce">
  <?php woocommerce_content(); ?>
</div>

<?php 
if( is_product() ) {
//get_template_part('includes/section-v');
echo '
<div class="container-fluid mt-5 mb-5">
  <div class="row text-center">
    <div class="col-12">
      <div class="bk--title">
          <h2 class="text-center">Las mejores Experiencias<span class="bk--title__i"> Royal Enfield</span></h2>
          <p class="text-center">- - MADE LIKE A GUN - -</p>
      </div>
    </div>
    <div class="col-12">
';
$instagram_feed = get_field('shortcode_instagram_feed');
if ($instagram_feed){
  echo do_shortcode($instagram_feed);
}
echo '
    </div>
  </div>
</div>
';
}
get_template_part('includes/section-c');
get_template_part('includes/footer'); 
?>
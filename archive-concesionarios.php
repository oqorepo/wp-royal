<?php get_template_part('includes/header'); ?>

<img src="<?php bloginfo('template_directory')?>/assets/img/bk-header-default.jpg" alt="" style="width:100%;">
<section class="container-fluid">

    <div class="row bk-dealer--row" >

        <!--INICIO COLUMNA 1 SELECTOR-->
        <div class="bk-dealer--selector text-center w-100" id="style-2">

            <div class="bk-dealer--detail p-2">

                <span class="bk-dealer--icon"><i class="fas fa-map-marker-alt"></i></span>
                <h4 class="mt-3 mb-3">Seleccione su Región</h4>

                <div class="bk-dealer--select mb-5">
                <?php 
                $taxonomies = get_terms( array(
                    'taxonomy' => 'ubicaciones',
                    'hide_empty' => true
                ) );
                
                if ( !empty($taxonomies) ) :
                    $output = '<select name="selector_regiones_ces" id="js_selector_regiones_ces"><option>Seleccionar</option>';
                    foreach( $taxonomies as $category ) {
                        if( $category->parent == 0 ) {
                            $output.= '<optgroup label="'. esc_attr( $category->name ) .'">';
                            foreach( $taxonomies as $subcategory ) {
                                if($subcategory->parent == $category->term_id) {
                                $output.= '<option value="'.esc_attr(strtolower(str_replace(' ', '-',$subcategory->name ))).'">
                                    '. esc_html( $subcategory->name ) .'</option>';
                                }
                            }
                            $output.='</optgroup>';
                        }
                    }
                    $output.='</select>';
                    echo $output;
                endif;
                ?>
                </div>
                <hr>

                    <?php
                    $zona_norte_args = array(
                        'post_type'      => 'concesionarios',
                        'posts_per_page' => -1
                    );
                        $zona_norte = new WP_Query( $zona_norte_args ); 
                    ?>

                    <?php if ( $zona_norte->have_posts() ) :?>
                    
                        <ul class="bk-distribuidores">
                        <?php while ($zona_norte->have_posts()) : $zona_norte->the_post(); 
                            $location = get_field('cn-map');
                            $title = get_the_title();
                        ?>
                            <li class="<?php
                            $terms = get_the_terms( $post->ID, 'ubicaciones' );
                            if (!empty($terms)) {
                                foreach($terms as $term) {
                                    if (!get_term_children($term->term_id, 'ubicaciones')) {
                                        echo strtolower(str_replace(' ', '-',$term->name));
                                    }
                                }
                            }
                            ?> d-none">
                                
                                <div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>">
                                    <a class="bk-marker" data-name="<?php echo strtolower(str_replace(' ', '', the_title('', '', false))); ?>" href="#" rel="bookmark" data-marker="0" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>">
                                        <h4> <?php the_title(); ?></h4>
                                        <p class="address"><?php echo $location['address']; ?></p>
                                    </a>
                                </div>
                            </li>
                            <?php ?>
                        <?php endwhile; wp_reset_postdata();?>
                        </ul>
                    <?php endif;?>

            </div><!-- Detail card-->
        </div>
        <!--FIN COLUMNA 1 SELECTOR-->

        <!--INICIO COLUMNA 2 CONTENIDO-->
        <div class="bk-dealer--contenido d-none col-md-6 col-lg-3 bk-no-padding bk-shadow">
            <ul class="bk-dealer--card">
            <?php $data_args = array(
                'post_type'      => 'concesionarios',
                'posts_per_page' => -1
            );
            $data_loop = new WP_Query( $data_args ); 
            ?>
            <?php if ( $data_loop->have_posts() ) :?>
            <?php while ($data_loop->have_posts()) : $data_loop->the_post();
                $location = get_field('cn-map');
            ?>
                <li class="bk-dealer--content" id="<?php echo strtolower(str_replace(' ', '', the_title('', '', false))); ?>">
                <?php if( have_rows('cn-datos') ): while( have_rows('cn-datos') ): the_row();
                $image = get_sub_field('cn-img');
                $link = get_sub_field('cn-tel');
                $mail = get_sub_field('cn-email'); 
                $colors = get_sub_field('cn-service-type'); 
                ?>
                    <?php if( !empty($image) ): ?>
                    <div class="bk-dealer--card__img" style="background:url('<?php echo $image['url']; ?>');">
                    </div>
                    <?php endif; ?>

                    <div class="bk-dealer--card__txt">
                        <h4><?php the_title(); ?></h4>
                        <p class="cn-address"><b>Dirección:</b><br><?php echo $location['address']; ?></p>

                        <!-- <?php if( have_rows('cn-tel') ): ?>
                        <p class="cn-tel"><b>Teléfonos:</b><br>
                            <?php while( have_rows('cn-tel') ): the_row();
                            $content = get_sub_field('cn-tel-rp');
                        ?>
                            <a href="<?php echo $content ?>"> <?php echo $content ?> </a>
                            <?php endwhile; ?>
                        </p>
                        <?php endif; ?> -->

                        <?php if( have_rows('cn-email') ): ?>
                        <p class="cn-email"><b>Email:</b><br>
                            <?php while( have_rows('cn-email') ){
                                the_row(); 
                                $mailings = get_sub_field('cn-email-rp');
                                echo '<a href="mailto:'.$mailings.'">'.$mailings.'</a>';
                                break; 
                            } 
                        ?>
                        </p>
                        <?php endif; ?>

                        <?php 
                        
                        // check
                        if( $colors ): ?>
                            <p class="cn-service"><b>Tipos de servicios:</b><br></p>
                        <ul class="d-flex">
                            <?php foreach( $colors as $color ): ?>
                            <li class="cn-<?php echo $color; ?> cn-service-icon"><p><?php echo $color; ?></p></li>
                            <?php endforeach; ?>
                        </ul>
                        <?php endif; ?>

                    </div>
                <?php endwhile; endif; ?>
                </li>
            <?php endwhile; wp_reset_postdata(); ?>
            <?php endif;?>
            </ul>
        </div>
        <!--FIN COLUMNA 2 CONTENIDO-->
        
        <!--INICIO COLUMNA 3 MAP-->
        <div class="bk-dealer--map d-none col-lg-6 bk-no-padding">
            <?php  $args = array(
                'post_type'      => 'concesionarios',
                'posts_per_page' => -1
            );
            $the_query = new WP_Query($args);
            while ( $the_query->have_posts() ) : $the_query->the_post();
            $location = get_field('cn-map');
            ?>
            <div class="bk-attr" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>">
            </div>
            <?php endwhile; wp_reset_postdata();?>

            <?php if( !empty($location) ): ?>
            <div class="acf-map">
                <div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>">
                    <h4><a href="<?php the_permalink(); ?>" rel="bookmark"> <?php the_title(); ?></a></h4> <!-- Output the title -->
                    <p class="address"><?php echo $location['address']; ?></p> <!-- Output the address -->
                </div>
            </div>
            <?php endif; ?>
        </div>
        <!-- FINCOLUMNA 3 MAP-->

  </div><!-- /.row -->
</section><!-- /.container -->

<?php get_template_part('includes/map'); ?>
<?php get_template_part('includes/footer'); ?>
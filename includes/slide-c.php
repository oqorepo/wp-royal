<?php
	/* Cotizar */
global $product;
?>
	<div id="cd-nav" class="cd-nav">
		<div class="container cd-nav--container-">
			<div class="row justify-content-end">
				<div class="col-sm-6">
					<!-- <h3>Texto del cotizador</h3> -->
				</div>
				<div class="col-sm-2 text-right">
					<button class="hamburger hamburger--emphatic close-cd-nav-trigger">
						<span class="hamburger-box">
							<span class="hamburger-inner"></span>
						</span>
						<p>cerrar</p>
					</button>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 text-center">
					<div class="bk-cot--card">
						<div class="bk-cot--card__img" style="height:250px;width:auto;">
<?php 
	// mostramos imagen del producto (si estamos en la pagina del producto)
if (is_product()) {
	?>
							<img class="xw-100" src="<?php echo the_post_thumbnail_url("medium"); ?>" style="height:250px;width:auto;">
<?php 
	// si no hay moto seleccionada, mostramos imagen de moto por defecto
} else {
	?>
							<img src="<?php bloginfo('template_directory'); ?>/assets/img/bk-cot-moto-1.png" alt="" class="thumb_cotizar" style="height:250px;width:auto;">
<?php 
}
?>
						</div>
						<div class="bk-cot--card__content">
							<div class="bk-cot--card__content--price">
<?php 
if (is_product()) {
		// recogemos las variables de precio del producto
	if ($product->is_type('simple')) {
		$precio_normal = (int)get_post_meta(get_the_ID(), '_regular_price', true);;
		$precio_oferta = (int)get_post_meta(get_the_ID(), '_sale_price', true);
		if ($precio_oferta) {
			$valor_bono = ($precio_normal - $precio_oferta);
		} else {
			$valor_bono = (int)0;
		}
		$precio_final = ($precio_normal - $valor_bono);
	}

	if ($product->product_type == 'variable') {
					#Step 1: Get product varations
		$available_variations = $product->get_available_variations();
					#Step 2: Get product variation id
		$variation_id = $available_variations[0]['variation_id']; // Getting the variable id of just the 1st product. You can loop $available_variations to get info about each variation.
					#Step 3: Create the variable product object
		$variable_product1 = new WC_Product_Variation($variation_id);
					#Step 4: You have the data. Have fun :)
		$precio_normal = $variable_product1->regular_price;
		$precio_oferta = $variable_product1->sale_price;
		if ($precio_oferta) {
			$valor_bono = ($precio_normal - $precio_oferta);
		} else {
			$valor_bono = (int)0;
		}
		$precio_final = ($precio_normal - $valor_bono);
	}

	if (!empty($precio_oferta)) {
		?>
								<h4 class="name_moto"><?php echo the_title(); ?></h4>								
								<div class="bono bk-shop--banner__price-bono"><small>Precio Normal: <span style="color:#41b451; font-weight:900">$ <?php echo number_format($precio_normal, 0, ",", "."); ?></span></small></div>
							<?php 
					}
					?>
								<div class="normal bk-shop--banner__price-normal"><h2>$ <?php echo number_format($precio_final, 0, ",", "."); ?></h2></div>
							<?php
						if (!empty($precio_oferta)) {
							?>
								<div class="bono bk-shop--banner__price-bono"><h4>Incluye bono: $ <?php echo number_format($valor_bono, 0, ",", "."); ?></h4></div>
								
							<?php 
					}
				} else {
					?>
								<h4 class="name_moto"></h4>
								<div class="bono bk-shop--banner__price-bono" style="display:none;"><small>Precio Normal: <span style="color:#41b451; font-weight:900"></span></small></div>
								<div class="normal bk-shop--banner__price-normal"><h2><span></span></h2></div>
								<div class="bono bk-shop--banner__price-bono" style="display:none;"><h4>Incluye bono: $ <span></span></h4></div>
							<?php 
					}
					?>
							</div>
							<style>
									.bk-cot--card__content--cuote.autofin-evaluate .autofin-container .cae {
										font-weight: 700;
										color: #092841;
									}
									.bk-cot--card__content--cuote.autofin-evaluate .autofin-container .price {
										font-weight: 600;
										font-size: 24px;
										color: #41b451;
									}
									.bk-cot--card__content--cuote.autofin-evaluate .autofin-container {
										width: 70%;
										margin: 10px auto;
										background: #ddd;
										padding: 5px;
									}
								</style>
					<?php
				if (is_product()) {
							// si producto tiene autofin mostramos valor cuota
					$autofin_modelo = $product->get_attribute('autofin');
					$autofin_ano = $product->get_attribute('ano');
					$modelo_moto = $product->get_attribute('m-modelo');
					if (!empty($autofin_modelo)) {
						?>
							<div class="bk-cot--card__content--cuote autofin-evaluate" id="cuota_cae">
								<div class="autofin-container hidden"> 
									<div class="title black"><span class="cae"></span></div>
									<div class="price"></div>
									<input type="hidden" name="model" value="<?php echo $modelo_moto; ?>">
									<input type="hidden" name="use_autofin" value="">
									<input type="hidden" name="autofin_model" value="<?php echo $autofin_modelo; ?>">
									<input type="hidden" name="model_number" value="<?php echo $autofin_ano; ?>">
									<input type="hidden" name="value_vehicle" value="<?php echo $precio_normal; ?>">
									<input type="hidden" name="brand_vehicle" value="suzuki">
									<input type="hidden" name="value_bono" value="<?php echo $valor_bono; ?>">
									<input type="hidden" name="value_total" value="<?php echo $precio_final; ?>">
								</div>
							</div>
					<?php 
			}
		} else {
			?>
							<div class="bk-cot--card__content--cuote autofin-evaluate" id="cuota_cae">
								<div class="autofin-container hidden"> 
									<div class="title black"><span class="cae"></span></div>
									<div class="price"></div>
									<input type="hidden" name="model" value="">
									<input type="hidden" name="use_autofin" value="">
									<input type="hidden" name="autofin_model" value="">
									<input type="hidden" name="model_number" value="">
									<input type="hidden" name="value_vehicle" value="">
									<input type="hidden" name="brand_vehicle" value="suzuki">
									<input type="hidden" name="value_bono" value="">
									<input type="hidden" name="value_total" value="">
								</div>
							</div>
					<?php

			}
			?>
							<div class="p-2"><img src="<?php bloginfo('template_directory'); ?>/assets/img/bg-footer.jpg" alt="Suzuki" style="max-width:120px;"></div>

						</div>
					</div>
				</div>

				<div class="col-md-6">
					<?php 
				if (is_product() or is_shop()) {
					?>
					<div id="gracias-form-cotizar" style="display:none;">
						<h4>¡GRACIAS POR COTIZAR CON SUZUKI MOTOS!</h4>
						<p>Pronto serás contactado por un ejecutivo que te ayudará a complementar y a resolver todo tipo de duda adicional que tengas sobre tu cotización.</p>
						<div class="btn bk--btn bk--btn__primary" id="boton_reset_form"> ¿Quieres cotizar nuevamente? </div>
					</div>
					<div style="display:none;" id="caja_formulario_original">
						<?php echo do_shortcode('[contact-form-7 id="115" title="Cotización" html_id="formulario_original"]') ?>
					</div>
					<div role="form" class="wpcf7" id="wpcf7-f115-o1" lang="es-ES" dir="ltr">
						<form action="<?php if (is_product()) { echo esc_url($product->get_permalink()); } ?>" method="post" class="wpcf7-form" novalidate="novalidate" name ="formulario_cotizar" id="formulario_cotizar">
							<div style="display: none;">
								<input type="hidden" name="_wpcf7" value="115">
								<input type="hidden" name="_wpcf7_version" value="5.0.5">
								<input type="hidden" name="_wpcf7_locale" value="es_ES">
								<input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f115-o1">
								<input type="hidden" name="_wpcf7_container_post" value="0">
							<?php 
						if (is_product()) {
							?>
								<input type="hidden" name="moto_ano" value="<?php echo $autofin_ano; ?>" >
								<input type="hidden" name="moto_modelo" value="<?php echo $modelo_moto; ?>" >
								<input type="hidden" name="moto_autofin" value="<?php echo $autofin_modelo; ?>" >
								<input type="hidden" name="moto_sku" value="<?php echo $product->get_sku(); ?>" >
								<input type="hidden" name="moto_marca" value="Suzuki" >
								<input type="hidden" name="moto_cat" value="<?php 
								$terms = get_the_terms($product->ID, 'product_cat');
								foreach ($terms as $term) {
									$product_cat_name = $term->name;
									break;
								}
								echo $product_cat_name; ?>">
								<input type="hidden" name="moto_color" value="generic" >
								<input type="hidden" name="moto_thumb" value="<?php echo the_post_thumbnail_url("medium"); ?>" >
								<input type="hidden" name="moto_precio" value="<?php echo $precio_normal; ?>" >
								<input type="hidden" name="moto_bono" value="<?php echo $valor_bono; ?>" >
								<input type="hidden" name="moto_precio_final" value="<?php echo $precio_final; ?>" >
								<input type="hidden" name="moto_precio_p" value="" >
								<input type="hidden" name="moto_bono_p" value="" >
								<input type="hidden" name="moto_precio_final_p" value="" >
								<input type="hidden" name="moto_url" value="<?php echo esc_url($product->get_permalink()); ?>" >
								<input type="hidden" name="ces_autofin" value="no" >
								<input type="hidden" name="ces_email" value="" >
								<input type="hidden" name="ces_nombre" value="" >
								<input type="hidden" name="ces_comuna" value="" >
								<input type="hidden" name="zon_nombre" value="" >
								<input type="hidden" name="cot_origen" value="Modelo">
								<input type="hidden" name="url_cotizacion" value="">
							<?php 
					} else {
						?>
								<input type="hidden" name="moto_ano" value="" >
								<input type="hidden" name="moto_modelo" value="" >
								<input type="hidden" name="moto_autofin" value="" >
								<input type="hidden" name="moto_sku" value="" >
								<input type="hidden" name="moto_marca" value="Suzuki" >
								<input type="hidden" name="moto_cat" value="">
								<input type="hidden" name="moto_color" value="generic" >
								<input type="hidden" name="moto_thumb" value="" >
								<input type="hidden" name="moto_precio" value="" >
								<input type="hidden" name="moto_bono" value="" >
								<input type="hidden" name="moto_precio_final" value="" >
								<input type="hidden" name="moto_precio_p" value="" >
								<input type="hidden" name="moto_bono_p" value="" >
								<input type="hidden" name="moto_precio_final_p" value="" >
								<input type="hidden" name="moto_url" value="" >
								<input type="hidden" name="ces_autofin" value="no" >
								<input type="hidden" name="ces_email" value="" >
								<input type="hidden" name="ces_nombre" value="" >
								<input type="hidden" name="zon_nombre" value="" >
								<input type="hidden" name="cot_origen" value="Catalogo">
								<input type="hidden" name="url_cotizacion" value="">
							<?php 
					}
					?>
								<input type="hidden" name="cot_fecha" value="">
								<input type="hidden" name="utm_source" value="">
								<input type="hidden" name="utm_medium" value="">
								<input type="hidden" name="utm_campaign" value="">
								<input type="hidden" name="utm_content" value="">
								<input type="hidden" name="utm_term" value="">
								<input type="hidden" name="utm_code" value="">
							</div>
							<div class="form-row">
								<div class="wpcf7-response-output wpcf7-display-none"></div>
							</div>
							<div class="form-row">
								<?php
							if (is_product()) {
								if (!empty($autofin_modelo)) {
									?>
								<div class="form-group col-12 caja_opcion_financiar">
									<p><b>¿Necesita financiamiento?</b></p>
									<label class="bk-check--container">
										<input type="radio" name="financiamiento" value="evaluar" checked>SI
										<span class="checkmark"></span>
									</label>
									<label class="bk-check--container">
										<input type="radio" name="financiamiento" value="cotizar">NO
										<span class="checkmark"></span>
									</label>
								</div>
								<?php 
						} else {
							?>
									<label class="bk-check--container" style="display:none!important;">
										<input type="radio" name="financiamiento" value="evaluar">SI
										<span class="checkmark"></span>
									</label>
									<label class="bk-check--container" style="display:none!important;">
										<input type="radio" name="financiamiento" value="cotizar" checked>NO
										<span class="checkmark"></span>
									</label>
								<?php 
						}
					} else {
						?>
								<div class="form-group col-12 caja_opcion_financiar">
									<p><b>¿Necesita financiamiento?</b></p>
									<label class="bk-check--container">SI
										<input type="radio" name="financiamiento" value="evaluar">
										<span class="checkmark"></span>
									</label>
									<label class="bk-check--container">
										<input type="radio" name="financiamiento" value="cotizar">NO
										<span class="checkmark"></span>
									</label>
								</div>
								<?php 
						}
						?>
								<div class="col-12">
									<p><b>Información de Concesionario</b></p>
								</div>
								<div class="form-group col-md-6">
									<select name="ces_zona" id="bk_zona" class="wpcf7-form-control wpcf7-select wpcf7-validates-as-required form-control required" aria-required="true">
										<option value="0" selected>Seleccione su Región</option>
									<?php
								$taxonomy = 'ubicaciones';
								$terms = get_terms($taxonomy, array('parent' => 0)); // Get all top level terms of a taxonomy
								if ($terms && !is_wp_error($terms)) :
									foreach ($terms as $term) {
									?>
            							<option value="<?php echo $term->term_id; ?>"><?php echo $term->name; ?></option>
									<?php 
							}
							endif;
							?>
									</select>
								</div>
								<div class="form-group col-md-6">
									<select name="ces_comuna" id="bk_comuna" class="wpcf7-form-control wpcf7-select wpcf7-validates-as-required form-control required" aria-required="true">
										<option>---</option>

									</select>
								</div>
								<div class="form-group col-12">
									<select name="ces_concesionario" id="bk_concesionario" class="wpcf7-form-control wpcf7-select wpcf7-validates-as-required form-control required" aria-required="true">
										<option>---</option>
									</select>
								</div>
								<div class="form-group col-12 caja_nota_concesionario" style="display:none;">
									El Concesionario seleccionado no cuenta con opciones de financiamiento.
								</div>
							</div>

							<div class="form-row">
								<div class="col-12">
									<p><b>Datos personales</b></p>
								</div>
									
								<div class="form-group col-md-6">
									<input type="text" name="contacto_nombre" id="contacto_nombre" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control required" aria-required="true" placeholder="Nombre">
								</div>

								<div class="form-group col-md-6">
									<input type="text" name="contacto_apellido" id="contacto_apellido" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control required" aria-required="true" placeholder="Apellido">
								</div>

								<div class="form-group col-md-6">
									<input type="text" name="contacto_rut" id="contacto_rut" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control Rut required" aria-required="true" placeholder="Rut">
								</div>

								<div class="form-group col-md-6">
									<input type="tel" name="contacto_telefono" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-required wpcf7-validates-as-tel form-control required" aria-required="true" placeholder="Teléfono">
								</div>
								<div class="form-group col-12">
									<input type="email" name="contacto_email" id="contacto_email" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email form-control required" aria-required="true" placeholder="Email">
								</div>

							</div>
							<div class="text-right pb-5">
							<?php
						if (is_product()) {
							if (!empty($autofin_modelo)) {
								?>
								<button type="submit" class="btn bk--btn bk--btn__primary" id="boton_enviar"> Cotizar <?php echo $product->name; ?> y Solicitar evaluación > </button>
							<?php 
					} else {
						?>
								<button type="submit" class="btn bk--btn bk--btn__primary" id="boton_enviar"> Cotizar <?php echo $product->name; ?> > </button>
							<?php 
					}
				} else {
					?>
								<button type="submit" class="btn bk--btn bk--btn__primary" id="boton_enviar"> Cotizar y Solicitar evaluación > </button>
							<?php 
					}
					?>
								<span class="ajax-loader"></span>
							</div>
						</form>
					</div>
					<?php 
			} else {
				?>
					<h3><a href="/suzuki">ir al catalogo</a></h3>
					<?php	
			}
			?>
				</div>
			</div><!-- .row -->
		</div> <!-- .container -->
	</div> <!-- .cd-nav -->



<!-- Modal -->
<div class="modal fade" id="autofinModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index:9999999999;">
  <div class="modal-dialog modal-dialog-centered" >
    <div class="modal-content" style="padding:0;">
      <div class="modal-header" style="padding:5px 10px 0px;">
      	<h5 class="modal-title">Evaluación de Crédito Autofin</h5>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      </div>
      <div class="modal-body" style="padding:0;">
          <iframe id="autofinIframe" src="" width="100%" height="740px" frameborder="0" allowtransparency="true"></iframe>  
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
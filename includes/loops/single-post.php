<?php
/*
The Single Post
===============
*/
?>

<section class="bk-section">
<?php if(have_posts()): while(have_posts()): the_post(); ?>
    <article role="article" id="post_<?php the_ID()?>" <?php post_class()?>>
        <header class="text-center mb-4 container">
            <div class="row">
                <div class="col">
                    <?php the_post_thumbnail('full', array('class' => 'w-100')); ?>
                    <h2 class=" m-5 bk-title--red">
                        <?php the_title()?>
                    </h2>
                </div>
            </div>
        </header>
        <div class="container ">
            <div class="row bk-section">
                <div class="col">
                    <?php
                    the_content();
                    wp_link_pages();
                    ?>
                </div>
            </div>
    </article>

<?php
  endwhile; wp_reset_postdata(); else :
    get_template_part('./includes/loops/404');
  endif;
?>

<div class="container">
    <div class="row pt-4">
        <div class="col">
            <?php previous_post_link('%link', '<i class="fas fa-fw fa-arrow-left"></i> Evento Anterior: '.'%title'); ?>
        </div>
        <div class="col text-right">
            <?php next_post_link('%link', 'Siguiente Evento: '.'%title' . ' <i class="fas fa-fw fa-arrow-right"></i>'); ?>
        </div>
    </div>
</div>
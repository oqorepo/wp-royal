<?php $slider_args = array(
    'post_type'      => 'post',
    'posts_per_page' => 2
);
$slider = new WP_Query($slider_args);
if($slider->have_posts()):
$count = $slider->found_posts;
$count = 2;
?>
<section class=" bk-loop--novedades mb-5">
    <div class="container">
        <div class=" pt-5 bk-loop--novedades__slider">
            <div class="pt-5 pb-4 bk--title bk--title__white bk-loop--novedades__slider-title">
                <h2 class="d-inline">Últimas novedades</h2>
                <span class=" ml-5">
                    <a href="<?php bloginfo('url'); ?>/novedades" class="bk--btn bk--btn__line">Ver Todas</a>  
                </span>
            </div>
            <div id="main-slider" class="carousel slide mt-4" data-ride="carousel">


            <ol class="carousel-indicators">
                <?php for($i = 0; $i < $count ;  $i++) { ?>
                <li data-target="#main-slider" data-slide-to="<?php echo $i; ?>" class="<?php echo ($i == 0) ? 'active' : ''?>"></li>
                <?php } ?>
            </ol> <!--.carousel-indicators-->

            <div class="carousel-inner" role="listbox">

            <?php $i = 0; while($slider->have_posts()): $slider->the_post(); ?>
                <div class="carousel-item <?php echo ($i == 0) ? 'active' : ''?>">
                    <a href="<?php the_permalink(); ?>">
                        <div class="carousel-item--contenedor">
                            <?php the_post_thumbnail('medium_size_w', array(
                                'class' => 'bk-loop--novedades__slider-img w-100',
                                'alt' => get_the_title() ) ) ; ?>
                        </div>
                        <div class="bk-loop--novedades__slider-txt">
                            <h3 class="text-uppercase"><?php echo get_the_title(); ?></h3>
                            <p class="pb-4"><?php echo get_the_date('d / m / y'); ?></p>
                        </div>
                    </a>
                </div><!--.carousel-item-->
            <?php $i++; endwhile; ?>
            </div> <!--.carouse-inner-->


                <a href="#main-slider" class="carousel-control-prev" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a href="#main-slider" class="carousel-control-next" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>

            </div>
        </div>
    </div>
</section>
<?php endif;  wp_reset_postdata(); ?>
<?php

?>
<?php $slider_args = array(
    'post_type'      => 'product',
    'posts_per_page' => 5
);
$slider = new WP_Query($slider_args);
if($slider->have_posts()):
//$count = $slider->found_posts;
$count = 5;
?>
<div class="container">
    <div class="row">
        <div class="col pt-5 pb-5">
            <div class="bk--title">
                <h2 class="text-center">Elige la moto de <span class="bk--title__i">tus sueños</span></h2>
                <p class="text-center mb-0">- WAY OF LIFE -</p>
            </div>
        </div>
    </div>
</div>
<section class=" bk-loop--productos mb-5">
    <div class=" pb-5">
        <div class="bk-loop--productos__slider">
            <div id="product-slider" class="carousel slide mt-4" data-ride="carousel">


            <ol class="carousel-indicators">
                <?php for($i = 0; $i < $count ;  $i++) { ?>
                <li data-target="#product-slider" data-slide-to="<?php echo $i; ?>" class="<?php echo ($i == 0) ? 'active' : ''?>"></li>
                <?php } ?>
            </ol> <!--.carousel-indicators-->

            <div class="carousel-inner" role="listbox">

            <?php $i = 0; while($slider->have_posts()): $slider->the_post(); ?>
                <div class="carousel-item <?php echo ($i == 0) ? 'active' : ''?>">
                <div class="carousel-wrapper">
                    <a href="<?php the_permalink(); ?>">
                            <?php the_post_thumbnail( 'slider', array(
                                'class' => 'bk-loop--productos__slider-img img-fluid',
                                'alt' => get_the_title() ) ) ; ?>
                        <div class="bk-loop--productos__slider-txt">
                        <h3 class="text-uppercase bk-loop--productos__slider-title"><?php echo get_the_title(); ?></h3>
                        <?php $price = get_post_meta( get_the_ID(), '_price', true ); ?>
                        <h4 class="product-price-tickr">Desde <?php echo wc_price( $price ); ?></h4>
                        </div>
                    </a>
                </div><!--.carousel-item-->
                </div><!--.carousel-item-->
            <?php $i++; endwhile; wp_reset_postdata();?>
            </div> <!--.carouse-inner-->

               <!--  <a href="#product-slider" class="carousel-control-prev" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a href="#product-slider" class="carousel-control-next" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a> -->

            </div>
        </div>
    </div>
</section>
<?php endif;  wp_reset_query(); ?>
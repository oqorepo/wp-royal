<?php
/*
video
 */
if (is_front_page()):
?>
<section class="pb-5 bk-home--video">
    <div class="container pb-5">
        <div class="row">
            <div class="col-lg-6">
                <div class="bk--title ">
                    <h2 class="text-center">Pura actitud<span class="bk--title__i"> en la ruta</span></h2>
                    <p class="text-center">Cualquiera puede conducir. <br>Para pilotear, necesitas pasión.</p>
                </div>
                    <p class="text-center">Es en ese camino favorito, donde el pavimento se une a ti como un buen amigo. El tramo salvaje entre la naturaleza que te suplica salir y explorar. O la llamada de la pista de carreras que te levanta temprano los fines de semana. En Suzuki, construimos motocicletas porque compartimos tu entusiasmo por salir y partir. Para nosotros, todo esto es una forma de vida, y la vivirás aquí junto a nosotros.</p>
                <div class="text-center">
                    <a href="<?php echo site_url('/moto/gsx-r-1000r/'); ?>" class="bk--btn bk--btn__primary">Descubre más ></a>
                </div>
            </div>
            <a href="#" class="wp-video-popup bk-video-popup">
                <div class="bk-home--video__img bk-home--video__img-home">
                    <img class="bk-home--video__icon" src="<?php bloginfo('template_directory'); ?>/assets/img/bk-play-icon.png" alt="">
                </div>
            </a>
        </div>
    </div>
</section>
<?php echo do_shortcode('[wp-video-popup video="https://www.youtube.com/watch?v=nuR4oxxtSHU"]');?>
<?php endif; ?>

<?php
/*
video
 */
if (is_product()):
?>
<?php 
$videoUrl = get_field('enlace_video');
$videoImg = get_field('video_imagen');
$videoTitle = get_field('video_titulo');
$videoText = get_field('video_texto');
?>
<section class="pb-5 bk-home--video">
    <div class="container pb-5">
        <div class="row">
            <div class="col-lg-6">
                <div class="bk--title ">
                    <h2 class="text-center bk-video__title"><?php echo $videoTitle;?></h2>
                    <p class="text-center">- WAY OF LIFE -</p>
                </div>
                    <p class="text-center"><?php echo $videoText;?></p>
<!--                 <div class="text-center">
                    <a href="#" class="wp-video-popup bk--btn bk--btn__primary">Descubre más ></a>
                </div> -->
            </div>
            <div class="bk-home--video__img">
                <a href="#" class="wp-video-popup">
                    <img class="d-flex" src="<?php echo $videoImg['sizes'][ 'large' ];?>" alt="<?php echo $videoImg['alt'];?>">
                </a>
            </div>
        </div>
    </div>
</section>
<?php echo do_shortcode('[wp-video-popup video="https://youtu.be/'.$videoUrl.'"]');?>
<?php endif; ?>
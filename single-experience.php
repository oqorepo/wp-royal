<?php 
get_template_part('includes/header'); 
$today = date("Ymd");
$event_date =  get_field('ex_fecha_del_evento');?>
    
<section class="">
<?php if(have_posts()): while(have_posts()): the_post(); ?>
    <?php the_post_thumbnail('full', array('class' => 'w-100')); ?>
    <article role="article" id="post_<?php the_ID()?>" <?php post_class()?>>
        <header class="text-center mb-4 container">
            <div class="row">
                <div class="col">
                    <h2 class=" m-5 bk--title">
                        <?php the_title()?>
                    </h2>
                </div>
            </div>
        </header>
        <div class="container ">
            <div class="row bk-section">
                <div class="col">
                    <?php
                    the_content();
                    ?>
                </div>
            </div>
    </article>

<?php
  endwhile; wp_reset_postdata(); else :
    get_template_part('./includes/loops/404');
  endif;
?>
</section>

<?php if ($today <= $event_date):?>
<section class="container">
  <div class="row mt-5 pt-4">
    <div class="col-sm">
    <?php
    //=================================// CAMPO CRONOGRAMA //==================================//
    if( have_rows('ex_cronograma') ): ?>
      <h3 class="bk--title">Programación</h3>
      <p><small class="bk--text-primary-color">Cronograma</small></p>
      <hr>
      <ul class="">

        <?php 
        while( have_rows('ex_cronograma') ): the_row(); 
        $hora = get_sub_field('ex_hora_programada');
        $actividad = get_sub_field('ex_nombre_actividad');
        ?>

        <li class="pt-2 pb-2">
          <b>
            <?php echo $hora; ?>
          </b>
          <span> - </span>
          <span>
            <?php echo $actividad; ?>
          </span>
        </li>

        <?php endwhile; ?>
      </ul>
    <?php endif; ?>
    </div>

    <div class="col-sm">
    <?php //================================= CAMPO RUTAS //==================================
    $location = get_field('ex_ubicacion');
    if( !empty($location) ): ?>

      <h3 class="bk--title">Rutas</h3>
      <small class="bk--text-primary-color">Lugar del Evento</small>
      <hr>
      <div class="acf-map">
          <div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>">
              <h4><a href="<?php the_permalink(); ?>" rel="bookmark"> <?php the_title(); ?></a></h4> <!-- Output the title -->
              <p class="address"><?php echo $location['address']; ?></p> <!-- Output the address -->
          </div>
      </div>
      <?php endif; ?>
    </div>
  </div>

  <div class="row mt-5 pt-4">
    <div class="col-sm">
    <?php //================================= CAMPO EQUIPAMIENTO //==================================
    $equipamientos = get_field('ex_equipamiento');
    if( $equipamientos ): ?>
      <h3 class="bk--title">Equipamiento</h3>
      <small class="bk--text-primary-color">Todo lo necesario para el evento</small>
      <hr>
      <ul class="list-inline">
        <?php foreach( $equipamientos as $equipo ): ?>
        <li class="list-inline-item"><span class="bk-<?php echo strtolower($equipo); ?>"></span>
        <b> <?php echo $equipo; ?></b> 
        </li>
        <?php endforeach; ?>
      </ul>
      <?php endif; ?>
    </div>

    <div class="col-sm">
      <?php //================================= CAMPO CONDICIÓN //==================================
                      $condiciones = get_field('ex_condiciones');

                      if( $condiciones ): ?>
      <h3 class="bk--title">Condiciones</h3>
      <small class="bk--text-primary-color">Clima e ingreso al evento</small>
      <hr>
      <ul class="list-inline">
        <?php foreach( $condiciones as $condicion): ?>
        <li class="list-inline-item text-center"><span class="bk-<?php echo strtolower($condicion); ?>"></span>
         <b><?php echo $condicion; ?></b>
        </li>
        <?php endforeach; ?>
      </ul>
      <?php endif; ?>
    </div>
  </div>
</section>
<?php else:?>
<?php 
if( have_rows('ex_imagenes') ):?>
  <div class="container-fluid pt-5 pb-5 bk-sponsors">
      <div class="container">
          <h3 class="text-center bk--title">Sponsors</h3>
         <p class="text-center"><small class="bk--text-primary-color">Nuestros patrocinantes para este evento</small></p> 
          <hr>
          <ul class="d-flex justify-content-around">
          <?php while( have_rows('ex_imagenes') ): the_row(); 
          $image = get_sub_field('ex_imagen');
          $size1 = 'full'; // (thumbnail, medium, large, full or custom size)?>
                  <li class="">
                  <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
                  </li>
          <?php endwhile;?>
          </ul>
      </div>
  </div>
<?php endif;?>
<?php endif;?>
<?php 
if( have_rows('ex_sponsors') ):?>
  <div class="container-fluid pt-5 pb-5 bk-sponsors">
      <div class="container">
          <h3 class="text-center bk--title">Sponsors</h3>
         <p class="text-center"><small class="bk--text-primary-color">Nuestros patrocinantes para este evento</small></p> 
          <hr>
          <ul class="d-flex justify-content-around">
          <?php while( have_rows('ex_sponsors') ): the_row(); 
          $image = get_sub_field('ex_sponsor_imagen');
          $size1 = 'full'; // (thumbnail, medium, large, full or custom size)?>
                  <li class="">
                  <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
                  </li>
          <?php endwhile;?>
          </ul>
      </div>
  </div>
<?php endif;?>


<?php get_template_part('includes/footer'); ?>